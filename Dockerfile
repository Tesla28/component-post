FROM openjdk:11
ADD target/docker-component-post.jar docker-component-post.jar
EXPOSE 7031
ENTRYPOINT ["java", "-jar", "docker-component-post.jar"]