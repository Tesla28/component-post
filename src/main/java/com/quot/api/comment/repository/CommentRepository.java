package com.quot.api.comment.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.CommentDTO;

@Repository
public interface CommentRepository extends MongoRepository<CommentDTO, Integer>
{

	void deleteById(String eachCommentId);

	CommentDTO findById(String string);

}
