package com.quot.api.auth.consumer;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.user.dto.UserDTO;

@Service
public class AuthConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthConsumer.class);
	
	@Autowired
	private AuthClient authClient;

	public boolean isTokenValid(String token) throws Exception
	{
		LOGGER.trace("in isTokenValid method of AuthConsumer class");
		boolean isTokenValid = false;
		@SuppressWarnings("rawtypes")
		ResponseEntity response = null;
		try
		{
			response = authClient.validateToken(token);
			LOGGER.trace("Response from auth component : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				@SuppressWarnings("unchecked")
				Map<String, Object> responseMap = (Map<String, Object>) response.getBody();
				isTokenValid = (boolean) responseMap.get("isTokenValid");
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in isTokenValid method : " + err.getMessage());
			isTokenValid = false;
		}

		LOGGER.trace("Response from isTokenValid : " + isTokenValid);
		return isTokenValid;
	}

	/**
	 * 
	 * Auth Consumer service method for getting userName from token
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public String getUsernameFromToken(String token) throws Exception
	{
		LOGGER.trace("in getUsernameFromToken method of AuthConsumer class");
		String usernameFromToken = null;

		try
		{
			String authToken = "Bearer " + token;
			ResponseEntity<Map<String, String>> response = authClient.getUsernameFromToken(token, authToken);
			LOGGER.trace("Response from auth component : " + response);
			if(response != null && response.getStatusCode().equals(HttpStatus.OK))
			{
				usernameFromToken = response.getBody().get("user");
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUsernameFromToken : " + err);
			usernameFromToken = null;
		}

		LOGGER.trace("Response from getUsernameFromToken : " + usernameFromToken);
		return usernameFromToken;
	}
	
	/**
     * 
     * Component auth consumer method to get current logged in user details
     * 
     * @author Akshay
     * @param token
     * @return
     */
    public UserDTO getCurrentUserFromToken(String token)
    {
        LOGGER.trace("in getCurrentUserFromToken method");
        ResponseEntity<Map<String, Object>> response = null;
        UserDTO userResponse = null;
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            response = authClient.getCurrentUserFromToken(token.substring(7), token);
            LOGGER.trace("Response from component-auth :  " + response);
            if (response.getStatusCode().equals(HttpStatus.OK))
            {
                LOGGER.trace("the response status code is 200 OK... user : " + response.getBody().get("user"));
                userResponse = mapper.convertValue(response.getBody().get("user"), UserDTO.class);
            }
        }
        catch (Exception err)
        {
            userResponse = null;
            LOGGER.error("Error getCurrentUserFromToken : " + err.getMessage());
        }

        LOGGER.trace("response from getCurrentUserFromToken : " + userResponse.toString());
        return userResponse;
    }

}
