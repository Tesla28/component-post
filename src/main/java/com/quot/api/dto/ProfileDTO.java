package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "userId", "username", "bio", "interests", "social", "location", "shelfs", "collaboratedShelfs", "invitations",
		"isCollaborationsAllowed", "followings", "followers", "date", "updatedDate" })
public class ProfileDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6699838322769880636L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("userId")
	private String userId;

	@JsonProperty("username")
	private String username;

	@JsonProperty("bio")
	private String bio;

	@JsonProperty("interests")
	private List<String> interests = null;

	@JsonProperty("social")
	private Map<String, String> social;

	@JsonProperty("location")
	private Location location;

	@JsonProperty("shelfs")
	private List<String> shelfs = null;
	
	@JsonProperty("isCollaborationsAllowed")
	private boolean isCollaborationsAllowed;

	@JsonProperty("collaboratedShelfs")
	private List<CollaborativeShelfDTO> collaboratedShelfs = null;

	@JsonProperty("invitations")
	private List<InvitationDTO> invitations = null;

	@JsonProperty("followings")
	private List<Following> followings = null;

	@JsonProperty("followers")
	private List<Follower> followers = null;

	@JsonProperty("date")
	private Date date;

	@JsonProperty("updatedDate")
	private Date updatedDate;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}

	@JsonProperty("bio")
	public String getBio()
	{
		return bio;
	}

	@JsonProperty("bio")
	public void setBio(String bio)
	{
		this.bio = bio;
	}

	@JsonProperty("interests")
	public List<String> getInterests()
	{
		return interests;
	}
	
	@JsonProperty("isCollaborationsAllowed")
	public boolean isCollaborationsAllowed()
	{
		return isCollaborationsAllowed;
	}
	
	@JsonProperty("isCollaborationsAllowed")
	public void setCollaborationsAllowed(boolean isCollaborationsAllowed)
	{
		this.isCollaborationsAllowed = isCollaborationsAllowed;
	}

	@JsonProperty("interests")
	public void setInterests(List<String> interests)
	{
		this.interests = interests;
	}

	@JsonProperty("social")
	public Map<String, String> getSocial()
	{
		return social;
	}

	@JsonProperty("social")
	public void setSocial(Map<String, String> social)
	{
		this.social = social;
	}

	@JsonProperty("location")
	public Location getLocation()
	{
		return location;
	}

	@JsonProperty("location")
	public void setLocation(Location location)
	{
		this.location = location;
	}

	@JsonProperty("collaboratedShelfs")
	public List<CollaborativeShelfDTO> getCollaboratedShelfs()
	{
		return collaboratedShelfs;
	}

	@JsonProperty("collaboratedShelfs")
	public void setCollaboratedShelfs(List<CollaborativeShelfDTO> collaboratedShelfs)
	{
		this.collaboratedShelfs = collaboratedShelfs;
	}

	@JsonProperty("invitations")
	public List<InvitationDTO> getInvitations()
	{
		return invitations;
	}

	@JsonProperty("invitations")
	public void setInvitations(List<InvitationDTO> invitations)
	{
		this.invitations = invitations;
	}

	@JsonProperty("shelfs")
	public List<String> getShelfs()
	{
		return shelfs;
	}

	@JsonProperty("shelfs")
	public void setShelfs(List<String> shelfs)
	{
		this.shelfs = shelfs;
	}

	@JsonProperty("followings")
	public List<Following> getFollowings()
	{
		return followings;
	}

	@JsonProperty("followings")
	public void setFollowings(List<Following> followings)
	{
		this.followings = followings;
	}

	@JsonProperty("followers")
	public List<Follower> getFollowers()
	{
		return followers;
	}

	@JsonProperty("followers")
	public void setFollowers(List<Follower> followers)
	{
		this.followers = followers;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}