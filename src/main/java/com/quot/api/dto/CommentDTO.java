package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userId", "commentText", "likes", "unlikes", "reports", "isEdited", "reply", "date", "updatedDate" })
@Document(collection = "user-post-comments")
public class CommentDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6014161394724356151L;

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("commentText")
	private String commentText;
	
	@JsonProperty("isEdited")
	private boolean isEdited;
	
	@JsonProperty("likes")
	private List<String> likes = null;
	
	@JsonProperty("unlikes")
	private List<String> unlikes = null;
	
	@JsonProperty("reports")
	private List<String> reports = null;
	
	@JsonProperty("reply")
	private Reply reply;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonProperty("updatedDate")
	private Date updatedDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("commentText")
	public String getCommentText()
	{
		return commentText;
	}

	@JsonProperty("commentText")
	public void setCommentText(String commentText)
	{
		this.commentText = commentText;
	}
	
	@JsonProperty("isEdited")
	public boolean isEdited()
	{
		return isEdited;
	}

	@JsonProperty("isEdited")
	public void setEdited(boolean isEdited)
	{
		this.isEdited = isEdited;
	}

	@JsonProperty("likes")
	public List<String> getLikes()
	{
		return likes;
	}

	@JsonProperty("likes")
	public void setLikes(List<String> likes)
	{
		this.likes = likes;
	}

	@JsonProperty("unlikes")
	public List<String> getUnlikes()
	{
		return unlikes;
	}

	@JsonProperty("unlikes")
	public void setUnlikes(List<String> unlikes)
	{
		this.unlikes = unlikes;
	}

	@JsonProperty("reports")
	public List<String> getReports()
	{
		return reports;
	}

	@JsonProperty("reports")
	public void setReports(List<String> reports)
	{
		this.reports = reports;
	}

	@JsonProperty("reply")
	public Reply getReply()
	{
		return reply;
	}

	@JsonProperty("reply")
	public void setReply(Reply reply)
	{
		this.reply = reply;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}