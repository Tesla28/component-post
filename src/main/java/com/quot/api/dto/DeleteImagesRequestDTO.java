package com.quot.api.dto;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "userName", "journalId", "images" })
public class DeleteImagesRequestDTO
{

	@JsonProperty("userName")
	private String userName;
	
	@JsonProperty("journalId")
	private String journalId;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("userName")
	public String getUserName()
	{
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	@JsonProperty("journalId")
	public String getJournalId()
	{
		return journalId;
	}

	@JsonProperty("journalId")
	public void setJournalId(String journalId)
	{
		this.journalId = journalId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}
	
	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}