package com.quot.api.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.quot.api.user.dto.UserDTO;

public class GetPostResponseWrapper implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 7784329669090094962L;

    @JsonProperty("post")
    private PostDTO post;
    
    @JsonProperty("postUser")
    private UserDTO user;

    @JsonProperty("post")
    public PostDTO getPost()
    {
        return post;
    }
    
    @JsonProperty("post")
    public void setPost(PostDTO post)
    {
        this.post = post;
    }

    @JsonProperty("postUser")
    public UserDTO getUser()
    {
        return user;
    }

    @JsonProperty("postUser")
    public void setUser(UserDTO user)
    {
        this.user = user;
    }
    
    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }
}
