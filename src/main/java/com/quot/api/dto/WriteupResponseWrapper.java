package com.quot.api.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "writeups" })
public class WriteupResponseWrapper
{

	@JsonProperty("writeups")
	private List<WriteUpDTO> writeups = new ArrayList<>();
	
	@JsonIgnore
	private int statusCode;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("writeups")
	public List<WriteUpDTO> getWriteups()
	{
		return writeups;
	}

	@JsonProperty("writeups")
	public void setWriteups(List<WriteUpDTO> writeups)
	{
		this.writeups = writeups;
	}
	
	@JsonIgnore
	public int getStatusCode()
	{
		return statusCode;
	}

	@JsonIgnore
	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this).append("writeups", writeups)
				.append("additionalProperties", additionalProperties).toString();
	}

}