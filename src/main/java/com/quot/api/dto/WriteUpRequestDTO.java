package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "username", "writeupTitle", "writeupText", "likes", "comments", "views" })
public class WriteUpRequestDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4848152621851920476L;

	@JsonProperty("username")
	private String username;
	
	@JsonProperty("writeupTitle")
	private String writeupTitle;
	
	@JsonProperty("writeupText")
	private String writeupText;
	
	@JsonProperty("likes")
	private List<String> likes = null;
	
	@JsonProperty("comments")
	private List<CommentDTO> comments = null;
	
	@JsonProperty("views")
	private Integer views;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	@JsonProperty("writeupTitle")
	public String getWriteupTitle()
	{
		return writeupTitle;
	}

	@JsonProperty("writeupTitle")
	public void setWriteupTitle(String writeupTitle)
	{
		this.writeupTitle = writeupTitle;
	}

	@JsonProperty("writeupText")
	public String getWriteupText()
	{
		return writeupText;
	}

	@JsonProperty("writeupText")
	public void setWriteupText(String writeupText)
	{
		this.writeupText = writeupText;
	}

	@JsonProperty("likes")
	public List<String> getLikes()
	{
		return likes;
	}

	@JsonProperty("likes")
	public void setLikes(List<String> likes)
	{
		this.likes = likes;
	}

	@JsonProperty("comments")
	public List<CommentDTO> getComments()
	{
		return comments;
	}

	@JsonProperty("comments")
	public void setComments(List<CommentDTO> comments)
	{
		this.comments = comments;
	}

	@JsonProperty("views")
	public Integer getViews()
	{
		return views;
	}

	@JsonProperty("views")
	public void setViews(Integer views)
	{
		this.views = views;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}