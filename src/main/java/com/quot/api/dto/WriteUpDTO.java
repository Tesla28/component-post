package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "username", "writeupTitle", "writeupText", "date", "updateDate", "likes", "comments", "views" })
@Document(collection = "writeups")
public class WriteUpDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6017601689960292120L;
	
	@Id
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("writeupTitle")
	private String writeupTitle;
	
	@JsonProperty("writeupText")
	private String writeupText;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonProperty("updateDate")
	private Date updateDate;
	
	@JsonProperty("likes")
	private List<String> likes = null;
	
	@JsonProperty("comments")
	private List<WriteUpComment> comments = null;
	
	@JsonProperty("views")
	private Integer views;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	@JsonProperty("writeupTitle")
	public String getWriteupTitle()
	{
		return writeupTitle;
	}

	@JsonProperty("writeupTitle")
	public void setWriteupTitle(String writeupTitle)
	{
		this.writeupTitle = writeupTitle;
	}

	@JsonProperty("writeupText")
	public String getWriteupText()
	{
		return writeupText;
	}

	@JsonProperty("writeupText")
	public void setWriteupText(String writeupText)
	{
		this.writeupText = writeupText;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updateDate")
	public Date getUpdateDate()
	{
		return updateDate;
	}

	@JsonProperty("updateDate")
	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	@JsonProperty("likes")
	public List<String> getLikes()
	{
		return likes;
	}

	@JsonProperty("likes")
	public void setLikes(List<String> likes)
	{
		this.likes = likes;
	}

	@JsonProperty("comments")
	public List<WriteUpComment> getComments()
	{
		return comments;
	}

	@JsonProperty("comments")
	public void setComments(List<WriteUpComment> comments)
	{
		this.comments = comments;
	}

	@JsonProperty("views")
	public Integer getViews()
	{
		return views;
	}

	@JsonProperty("views")
	public void setViews(Integer views)
	{
		this.views = views;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}