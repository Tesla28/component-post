package com.quot.api.dto;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "user", "commentText", "date", "updatedDate", "hidden" })
public class WriteUpComment
{

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("user")
	private User user;
	
	@JsonProperty("commentText")
	private String commentText;
	
	@JsonProperty("date")
	private String date;
	
	@JsonProperty("updatedDate")
	private String updatedDate;
	
	@JsonProperty("hidden")
	private Boolean hidden;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("commentId")
	public String getCommentId()
	{
		return id;
	}

	@JsonProperty("commentId")
	public void setCommentId(String id)
	{
		this.id = id;
	}

	@JsonProperty("username")
	public User getUsername()
	{
		return user;
	}

	@JsonProperty("username")
	public void setUsername(User user)
	{
		this.user = user;
	}

	@JsonProperty("commentText")
	public String getCommentText()
	{
		return commentText;
	}

	@JsonProperty("commentText")
	public void setCommentText(String commentText)
	{
		this.commentText = commentText;
	}

	@JsonProperty("date")
	public String getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(String date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public String getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(String updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonProperty("hidden")
	public Boolean getHidden()
	{
		return hidden;
	}

	@JsonProperty("hidden")
	public void setHidden(Boolean hidden)
	{
		this.hidden = hidden;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
