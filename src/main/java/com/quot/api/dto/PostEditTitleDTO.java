package com.quot.api.dto;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder(
{ "postId", "userId", "newTitle"})
public class PostEditTitleDTO
{

	@JsonProperty("postId")
	private String postId;
	
	@JsonProperty("userId")
	private String userId; 
	
	@JsonProperty("newTitle")
	private String newTitle;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("postId")
	public String getPostId()
	{
		return postId;
	}

	@JsonProperty("postId")
	public void setPostId(String postId)
	{
		this.postId = postId;
	}
	
	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}
	
	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("newTitle")
	public String getNewTitle()
	{
		return newTitle;
	}

	@JsonProperty("newTitle")
	public void setNewTitle(String Title)
	{
		this.newTitle = Title;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}
	
	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}