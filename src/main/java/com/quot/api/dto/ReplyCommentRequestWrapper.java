package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "commentId", "replyText" })
public class ReplyCommentRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3546980115169395351L;
	
	@JsonProperty("commentId")
	private String commentId;
	
	@JsonProperty("replyText")
	private String replyText;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("commentId")
	public String getCommentId()
	{
		return commentId;
	}

	@JsonProperty("commentId")
	public void setCommentId(String commentId)
	{
		this.commentId = commentId;
	}

	@JsonProperty("replyText")
	public String getReplyText()
	{
		return replyText;
	}

	@JsonProperty("replyText")
	public void setReplyText(String replyText)
	{
		this.replyText = replyText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}