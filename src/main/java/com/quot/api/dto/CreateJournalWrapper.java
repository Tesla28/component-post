package com.quot.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"journalTitle", "journalContent", "hashtags", "shouldPost", "journalDate"})
public class CreateJournalWrapper implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -5483824504916899673L;

    @JsonProperty("journalTitle")
    private String journalTitle;

    @JsonProperty("journalContent")
    private String journalContent;

    @JsonProperty("hashtags")
    private List<String> hashtags = new ArrayList<>();
    
    @JsonProperty("shouldPost")
    private boolean shouldPost;
    
    @JsonProperty("journalDate")
    private Date journalDate;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("journalTitle")
    public String getJournalTitle()
    {
        return journalTitle;
    }

    @JsonProperty("journalTitle")
    public void setJournalTitle(String journalTitle)
    {
        this.journalTitle = journalTitle;
    }

    @JsonProperty("journalContent")
    public String getJournalContent()
    {
        return journalContent;
    }

    @JsonProperty("journalContent")
    public void setJournalContent(String journalContent)
    {
        this.journalContent = journalContent;
    }

    @JsonProperty("hashtags")
    public List<String> getHashtags()
    {
        return hashtags;
    }

    @JsonProperty("hashtags")
    public void setHashtags(List<String> hashtags)
    {
        this.hashtags = hashtags;
    }

    @JsonProperty("shouldPost")
    public boolean isShouldPost()
    {
        return shouldPost;
    }

    @JsonProperty("shouldPost")
    public void setShouldPost(boolean shouldPost)
    {
        this.shouldPost = shouldPost;
    }
    
    @JsonProperty("journalDate")
    public Date getJournalDate()
    {
        return journalDate;
    }

    @JsonProperty("journalDate")
    public void setJournalDate(Date journalDate)
    {
        this.journalDate = journalDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }

}
