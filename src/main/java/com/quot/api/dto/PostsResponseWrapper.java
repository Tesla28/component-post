package com.quot.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "quotes" })
public class PostsResponseWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7556921865711102227L;

	@JsonProperty("quotes")
	private List<PostDTO> posts = new ArrayList<>();
	
	@JsonIgnore
	private int statusCode;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("quotes")
	public List<PostDTO> getPosts()
	{
		return posts;
	}

	@JsonProperty("quotes")
	public void setPosts(List<PostDTO> posts)
	{
		this.posts = posts;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}
	
	@JsonIgnore
	public int getStatusCode()
	{
		return statusCode;
	}

	@JsonIgnore
	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}