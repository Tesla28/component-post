package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"replyText", "userId", "username", "name", "avatar", "date", "updatedDate"})
public class Reply implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -2117170948659935384L;

    @JsonProperty("replyText")
    private String replyText;

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("username")
    private String username;

    @JsonProperty("name")
    private String name;

    @JsonProperty("avatar")
    private String avatar;

    @JsonProperty("date")
    private Date date;

    @JsonProperty("updatedDate")
    private Date updatedDate;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("replyText")
    public String getReplyText()
    {
        return replyText;
    }

    @JsonProperty("replyText")
    public void setReplyText(String replyText)
    {
        this.replyText = replyText;
    }

    @JsonProperty("userId")
    public String getUserId()
    {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @JsonProperty("username")
    public String getUsername()
    {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username)
    {
        this.username = username;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("avatar")
    public String getAvatar()
    {
        return avatar;
    }

    @JsonProperty("avatar")
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    @JsonProperty("date")
    public Date getDate()
    {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Date date)
    {
        this.date = date;
    }

    @JsonProperty("updatedDate")
    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    @JsonProperty("updatedDate")
    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }

}
