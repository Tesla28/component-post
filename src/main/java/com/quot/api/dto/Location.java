package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "city", "state", "country" })
public class Location implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1570584370875518775L;

	@JsonProperty("city")
	private String city;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("country")
	private String country;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city)
	{
		this.city = city;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	@JsonProperty("state")
	public void setState(String state)
	{
		this.state = state;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country)
	{
		this.country = country;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}