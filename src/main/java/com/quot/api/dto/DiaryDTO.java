package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "userId", "userName", "diaryDisplayPicture", "diaryName", "journalCount", "journals", "date",
		"updatedDate" })
@Document(collection = "dairies")
public class DiaryDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5583045067290521227L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("userId")
	private String userId;

	@JsonProperty("userName")
	private String userName;

	@JsonProperty("diaryDisplayPicture")
	private String diaryDisplayPicture;

	@JsonProperty("diaryName")
	private String diaryName;

	@JsonProperty("journalCount")
	private Integer journalCount;

	@JsonProperty("journals")
	private List<String> journals = null;

	@JsonProperty("date")
	private Date date;

	@JsonProperty("updatedDate")
	private Date updatedDate;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("userName")
	public String getUserName()
	{
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	@JsonProperty("diaryDisplayPicture")
	public String getDiaryDisplayPicture()
	{
		return diaryDisplayPicture;
	}

	@JsonProperty("diaryDisplayPicture")
	public void setDiaryDisplayPicture(String diaryDisplayPicture)
	{
		this.diaryDisplayPicture = diaryDisplayPicture;
	}

	@JsonProperty("diaryName")
	public String getDiaryName()
	{
		return diaryName;
	}

	@JsonProperty("diaryName")
	public void setDiaryName(String diaryName)
	{
		this.diaryName = diaryName;
	}

	@JsonProperty("journalCount")
	public Integer getJournalCount()
	{
		return journalCount;
	}

	@JsonProperty("journalCount")
	public void setJournalCount(Integer journalCount)
	{
		this.journalCount = journalCount;
	}

	@JsonProperty("journals")
	public List<String> getJournals()
	{
		return journals;
	}

	@JsonProperty("journals")
	public void setJournals(List<String> journals)
	{
		this.journals = journals;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}