package com.quot.api.dto;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({ "postId", "pageNumber", "userId", "newText", "pageTitle" })
public class PagedPostEditTextDTO
{

	@JsonProperty("postId")
	private String postId;
	
	@JsonProperty("pageNumber")
	private int pageNumber;
	
	@JsonProperty("userId")
	private String userId; 
	
	@JsonProperty("pageTitle")
	private String pageTitle; 
	
	@JsonProperty("newText")
	private String newText;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("postId")
	public String getPostId()
	{
		return postId;
	}

	@JsonProperty("postId")
	public void setPostId(String postId)
	{
		this.postId = postId;
	}
	
	@JsonProperty("pageNumber")
	public int getPageNumber()
	{
		return pageNumber;
	}

	@JsonProperty("pageNumber")
	public void setPageNumber(int pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}
	
	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	@JsonProperty("pageTitle")
	public String getPageTitle()
	{
		return pageTitle;
	}

	@JsonProperty("pageTitle")
	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	@JsonProperty("newText")
	public String getNewText()
	{
		return newText;
	}

	@JsonProperty("newText")
	public void setNewText(String newText)
	{
		this.newText = newText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}
	
	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}