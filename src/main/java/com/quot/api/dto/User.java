package com.quot.api.dto;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "userId", "userName", "avatar" })
public class User
{

	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("userName")
	private String userName;
	
	@JsonProperty("avatar")
	private String avatar;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("userName")
	public String getUserName()
	{
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	@JsonProperty("avatar")
	public String getAvatar()
	{
		return avatar;
	}

	@JsonProperty("avatar")
	public void setAvatar(String avatar)
	{
		this.avatar = avatar;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
