package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "username", "writeupId", "updatedWriteUpText" })
public class EditWriteUpRequestDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6029754640248517866L;

	@JsonProperty("username")
	private String username;

	@JsonProperty("writeupId")
	private String writeupId;

	@JsonProperty("updatedWriteUpText")
	private String updatedWriteUpText;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}

	@JsonProperty("writeupId")
	public String getWriteupId()
	{
		return writeupId;
	}

	@JsonProperty("writeupId")
	public void setWriteupId(String writeupId)
	{
		this.writeupId = writeupId;
	}

	@JsonProperty("updatedWriteUpText")
	public String getUpdatedWriteUpText()
	{
		return updatedWriteUpText;
	}

	@JsonProperty("updatedWriteUpText")
	public void setUpdatedWriteUpText(String updatedWriteUpText)
	{
		this.updatedWriteUpText = updatedWriteUpText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}