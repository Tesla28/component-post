package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shelfId", "invitedBy", "noOfCollaboraters", "date" })
public class InvitationDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2012241551523781956L;

	@JsonProperty("shelfId")
	private String shelfId;
	
	@JsonProperty("invitedBy")
	private String invitedBy;
	
	@JsonProperty("noOfCollaboraters")
	private Integer noOfCollaboraters;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("shelfId")
	public String getShelfId()
	{
		return shelfId;
	}

	@JsonProperty("shelfId")
	public void setShelfId(String shelfId)
	{
		this.shelfId = shelfId;
	}

	@JsonProperty("invitedBy")
	public String getInvitedBy()
	{
		return invitedBy;
	}

	@JsonProperty("invitedBy")
	public void setInvitedBy(String invitedBy)
	{
		this.invitedBy = invitedBy;
	}

	@JsonProperty("noOfCollaboraters")
	public Integer getNoOfCollaboraters()
	{
		return noOfCollaboraters;
	}

	@JsonProperty("noOfCollaboraters")
	public void setNoOfCollaboraters(Integer noOfCollaboraters)
	{
		this.noOfCollaboraters = noOfCollaboraters;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}