package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"id", "userId", "journalTitle", "journalContent", "hashtags", "isPosted", "journalDate",
    "journalMonth", "journalYear", "date", "updatedDate"})
@Document(collection = "user-jounals")
public class JournalDTO implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -6645652898812849478L;

    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("journalTitle")
    private String journalTitle;

    @JsonProperty("journalContent")
    private String journalContent;

    @JsonProperty("hashtags")
    private List<String> hashtags = null;

    @JsonProperty("isPosted")
    private Boolean isPosted;

    @JsonProperty("journalDate")
    private Date journalDate;

    @JsonProperty("journalMonth")
    private String journalMonth;

    @JsonProperty("journalYear")
    private String journalYear;

    @JsonProperty("date")
    private Date date;

    @JsonProperty("updatedDate")
    private Date updatedDate;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId()
    {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id)
    {
        this.id = id;
    }

    @JsonProperty("userId")
    public String getUserId()
    {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @JsonProperty("journalTitle")
    public String getJournalTitle()
    {
        return journalTitle;
    }

    @JsonProperty("journalTitle")
    public void setJournalTitle(String journalTitle)
    {
        this.journalTitle = journalTitle;
    }

    @JsonProperty("journalContent")
    public String getJournalContent()
    {
        return journalContent;
    }

    @JsonProperty("journalContent")
    public void setJournalContent(String journalContent)
    {
        this.journalContent = journalContent;
    }

    @JsonProperty("hashtags")
    public List<String> getHashtags()
    {
        return hashtags;
    }

    @JsonProperty("hashtags")
    public void setHashtags(List<String> hashtags)
    {
        this.hashtags = hashtags;
    }

    @JsonProperty("isPosted")
    public Boolean getIsPosted()
    {
        return isPosted;
    }

    @JsonProperty("isPosted")
    public void setIsPosted(Boolean isPosted)
    {
        this.isPosted = isPosted;
    }

    @JsonProperty("journalDate")
    public Date getJournalDate()
    {
        return journalDate;
    }

    @JsonProperty("journalDate")
    public void setJournalDate(Date journalDate)
    {
        this.journalDate = journalDate;
    }

    @JsonProperty("journalMonth")
    public String getJournalMonth()
    {
        return journalMonth;
    }

    @JsonProperty("journalMonth")
    public void setJournalMonth(String journalMonth)
    {
        this.journalMonth = journalMonth;
    }

    @JsonProperty("journalYear")
    public String getJournalYear()
    {
        return journalYear;
    }

    @JsonProperty("journalYear")
    public void setJournalYear(String journalYear)
    {
        this.journalYear = journalYear;
    }

    @JsonProperty("date")
    public Date getDate()
    {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Date date)
    {
        this.date = date;
    }

    @JsonProperty("updatedDate")
    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    @JsonProperty("updatedDate")
    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }

}
