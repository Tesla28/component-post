package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "commentId", "postId", "newCommentText" })
public class CommentEditWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2300501102505708743L;

	@JsonProperty("commentId")
	private String commentId;
	
	@JsonProperty("postId")
	private String postId;
	
	@JsonProperty("newCommentText")
	private String newCommentText;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("commentId")
	public String getCommentId()
	{
		return commentId;
	}

	@JsonProperty("commentId")
	public void setCommentId(String commentId)
	{
		this.commentId = commentId;
	}

	@JsonProperty("postId")
	public String getPostId()
	{
		return postId;
	}

	@JsonProperty("postId")
	public void setPostId(String postId)
	{
		this.postId = postId;
	}

	@JsonProperty("newCommentText")
	public String getNewCommentText()
	{
		return newCommentText;
	}

	@JsonProperty("newCommentText")
	public void setNewCommentText(String newCommentText)
	{
		this.newCommentText = newCommentText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
