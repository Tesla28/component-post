package com.quot.api.dto;

import java.util.Map;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "postId", "comment" })
public class CommentRequestDTO
{

	@JsonProperty("postId")
	private String postId;

	@JsonProperty("comment")
	private CommentDTO comment;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = null;

	@JsonProperty("postId")
	public String getPostId()
	{
		return postId;
	}

	@JsonProperty("postId")
	public void setPostId(String postId)
	{
		this.postId = postId;
	}

	@JsonProperty("comment")
	public CommentDTO getComment()
	{
		return comment;
	}

	@JsonProperty("comment")
	public void setComment(CommentDTO comment)
	{
		this.comment = comment;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

}
