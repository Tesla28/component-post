package com.quot.api.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.quot.api.constant.PostConstants;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.PostsResponseWrapper;

@Service
public class RedisCacheService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RedisCacheService.class);

	/**
	 * 
	 * Service method to add user post to its followers cache 
	 * 
	 * @author Akshay
	 * @param eachFollower
	 * @param postRequest
	 */
	@Cacheable(key = "#eachFollower", value = PostConstants.REDIS_USER_DASHBOARD_VALUE)
	public PostsResponseWrapper addPostToFollowerCache(String eachFollower, PostDTO postRequest)
	{
		LOGGER.trace("in addPostToFollowerCache service method");
		PostsResponseWrapper responseWrapper = new PostsResponseWrapper();
		List<PostDTO> postList = new ArrayList<>();
		postList.add(postRequest);
		responseWrapper.setPosts(postList);
		return responseWrapper;
	}
	
	
}
