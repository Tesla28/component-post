package com.quot.api.service.helper;

import java.util.Comparator;

import org.springframework.stereotype.Service;

import com.quot.api.dto.PostDTO;

@Service
public class PostDateComparator implements Comparator<PostDTO>
{

	@Override
	public int compare(PostDTO o1, PostDTO o2)
	{
		return o1.getDate().compareTo(o2.getDate());
	}

}
