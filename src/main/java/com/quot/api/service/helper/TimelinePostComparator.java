package com.quot.api.service.helper;

import java.util.Comparator;

import com.quot.api.redis.timeline.entity.TimelineEntity;

public class TimelinePostComparator implements Comparator<TimelineEntity>
{

    @Override
    public int compare(TimelineEntity o1, TimelineEntity o2)
    {
        return o1.getDate().compareTo(o2.getDate());
    }

}
