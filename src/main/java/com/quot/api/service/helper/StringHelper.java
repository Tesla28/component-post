package com.quot.api.service.helper;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Base64;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class StringHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(StringHelper.class);

//	@Value("${image.medium.compression.factor}")
//	private String mediumCompressionFactor;
//
//	@Value("${image.high.compression.factor}")
//	private String highCompressionFactor;

	/**
	 * 
	 * Helper method to check if the requested parameter string is null or empty
	 * 
	 * @param s
	 * @return boolean isNullOrEmptyString
	 */
	public boolean isNullOrEmptyString(String s)
	{
		LOGGER.trace("in isNullOrEmptyString helper... requested string : " + s);
		boolean isNullOrEmptyString = false;
		if(s == null || s == "" || s.length() == 0)
		{
			isNullOrEmptyString = true;
		}

		LOGGER.trace("response from isNullOrEmptyString : " + isNullOrEmptyString);
		return isNullOrEmptyString;
	}

	/**
	 * 
	 * Helper method to highly compress a requested base64 Image string
	 * 
	 * @author Akshay
	 * @param base64ImageString
	 * @param username
	 * @return compressed image string
	 */
	@Deprecated
	public String getHighlyCompressedImageString(String base64ImageString, String username)
	{
		LOGGER.trace("in getCompressedImageString helper method");
		String mediumCompressedImageString = null;
		String sourceFileName = null;
		String destinationFileName = null;
		String initialPart = null;

		// Check if requested image is empty
		if(isNullOrEmptyString(base64ImageString))
		{
			LOGGER.trace("requested image is null or empty");
			mediumCompressedImageString = "";
			return mediumCompressedImageString;
		}

		try
		{
			LOGGER.trace("Starting to compress image string");
			
			initialPart = base64ImageString.substring(0, base64ImageString.indexOf(',') + 1);
			LOGGER.trace("initialPart : " + initialPart);
			String requestedImageString = base64ImageString.substring(base64ImageString.indexOf(',') + 1);
			LOGGER.trace("requested Image String sliced : " + requestedImageString);
			// Create source file
			sourceFileName = "highly-uncompressed-" + username + "-" + getCurrentTime() + ".jpeg";
			LOGGER.trace("source filename : " + sourceFileName);
			File sourceFile = new File(
					"C:\\Users\\Akshay\\Desktop\\quote_repo_local\\component-post\\" + sourceFileName);

			byte[] decodedBytes = Base64.getDecoder().decode(requestedImageString);
			LOGGER.trace("decodede byte arary : " + decodedBytes);
			FileUtils.writeByteArrayToFile(sourceFile, decodedBytes);
			LOGGER.trace("base64 encoded image string written to source file");

			RenderedImage image = ImageIO.read(sourceFile);
			System.out.println("image : " + image);
			ImageWriter imageWriter = ImageIO.getImageWritersByFormatName("jpeg").next();
			ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
			imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			// imageWriteParam.setCompressionQuality(Float.valueOf(highCompressionFactor));

			// Create destination file
			destinationFileName = "high-compressed" + username + "-" + getCurrentTime() + ".png";
			LOGGER.trace("destination filename : " + destinationFileName);
			File destinationFile = new File("./" + destinationFileName);

			// Compression process
			try (ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(destinationFile))
			{
				LOGGER.trace("compressing image... start time - " + Instant.now());
				imageWriter.setOutput(imageOutputStream);
				IIOImage outputImage = new IIOImage(image, null, null);
				imageWriter.write(null, outputImage, imageWriteParam);

				imageWriter.dispose();
				LOGGER.trace("compression compete... end time - " + Instant.now());
			}

			// Get string data from destination file
			LOGGER.trace("Encoding compressed image file in base64 format");
			byte[] destinationfileContent = FileUtils.readFileToByteArray(destinationFile);
			mediumCompressedImageString = Base64.getEncoder().encodeToString(destinationfileContent);
			LOGGER.trace("Compressed encoded string : " + mediumCompressedImageString);

			// Delete source and destination files
			sourceFile.delete();
			destinationFile.delete();

			LOGGER.trace("image compression complete");

		}
		catch (IOException ioErr)
		{
			LOGGER.error("IOError in getCompressedImageString : " + ioErr.getMessage());
			return null;
		}
		catch (Exception compressionErr)
		{
			LOGGER.error("Error in getCompressedImageString : " + compressionErr.getMessage());
			return null;
		}

		return initialPart + mediumCompressedImageString;
	}

	/**
	 * 
	 * Helper method to get current milliseconds from epoch time
	 * 
	 * @author Akshay
	 * @return String of current milliseconds
	 */
	public String getCurrentTime()
	{
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return String.valueOf(timestamp.getTime());
	}
}
