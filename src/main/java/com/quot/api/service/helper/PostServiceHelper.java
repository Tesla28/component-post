package com.quot.api.service.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.constant.PostConstants;
import com.quot.api.dto.PageDTO;
import com.quot.api.dto.PostDTO;
import com.quot.api.user.consumer.UserConsumer;
import com.quot.api.user.dto.UserDTO;

@Service
public class PostServiceHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PostServiceHelper.class);
	
	@Autowired
	private AuthConsumer authConsumer;
	
	@Autowired
	private UserConsumer userConsumer;
	
	/**
	 * Helper method to check if logged in user is posting
	 * 
	 * @param token
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public boolean isAllowedToModify(String token, String username) throws Exception
	{
		boolean isAllowedToAdd = false;
		
		String userNameFromToken = authConsumer.getUsernameFromToken(token);
		LOGGER.trace("userName from token : " + userNameFromToken);
		
		if(userNameFromToken != null && userNameFromToken!="")
		{
			isAllowedToAdd = userNameFromToken.equals(username);
		}
		
		LOGGER.trace("is current user allowed to modify : " + isAllowedToAdd);
		return isAllowedToAdd;
	}
	
	
	/**
	 * 
	 * Helper method to check of the requested userId is allowed to perform a specified action of delete
	 * 
	 * @author akshay
	 * @param substring
	 * @param userId
	 * @return
	 */
	public boolean isAllowedToDelete(String token, String userId)
	{
		LOGGER.trace("in isAllowedToDelete... userId : " + userId);
		boolean isAllowedToDelete = false;
		
		UserDTO user = userConsumer.getUserFromToken(token);
		if(user==null)
		{
			LOGGER.trace("User not found... isAllowedToDelete : " + isAllowedToDelete);
			isAllowedToDelete = false;
		}
		else
		{
			LOGGER.trace("user : " + user.toString());
			if(user.getId().equals(userId))
			{
				LOGGER.trace("both the userIds matches");
				isAllowedToDelete = true;
			}
		}
		
		LOGGER.trace("Response from isAllowedToDelete : " + isAllowedToDelete);
		return isAllowedToDelete;
	}
	
	/**
	 * 
	 * Helper method to determine if requested user is allowed to edit requested post text or not
	 * 
	 * @author akshay
	 * @param token
	 * @param userId
	 * @return
	 */
	public boolean isAllowedToModifyPostText(PostDTO post, String userId)
	{
		LOGGER.trace("in isAllowedToModifyPostText : " + userId);
		boolean isAllowedToModifyPostText = false;
		
		if(post.getUserId().equals(userId))
		{
			LOGGER.trace("User is allowed to edit the post text");
			isAllowedToModifyPostText = true;
		}
		
		LOGGER.trace("Response from isAllowedToModifyPostText : " + isAllowedToModifyPostText);
		return isAllowedToModifyPostText;
	}
	
	/**
	 * Helper method to get the formatted date from the input date
	 * 
	 * @author Akshay
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public Date parseFormattedDate() throws ParseException
	{
		Date currentDate = new Date();
		LOGGER.trace("current unformatted date : " + currentDate);
		String datePattern = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
		String parsedDateString = sdf.format(currentDate);
		Date parsedDate = sdf.parse(parsedDateString);
		LOGGER.trace("current formatted date : " + parsedDate);
		return parsedDate;
	}

	/**
	 * 
	 * Helper method to generate a unique dairyName
	 * 
	 * @author Akshay
	 * @param username
	 * @return
	 */
	public String generateDiaryName(String username)
	{
		return "My Dairy-" + username + "-"+ Instant.now().toEpochMilli();
	}
	
	/**
	 * 
	 * Common Helper method to check if the header token is valid or not 
	 * 
	 * @author Akshay
	 * @param token
	 * @return boolean isTokenValid
	 * @throws Exception
	 */
	public boolean isTokenValid(String token) throws Exception
	{
		return authConsumer.isTokenValid(token);
	}

	/**
	 * 
	 * Helper method to calculate the number of words for requested string parameter
	 * 
	 * @author akshay
	 * @param text
	 * @return int length
	 */
	public int getWordLength(String text)
	{
		LOGGER.trace("in helper getWordLength method... requested text : " + text);
		int wordLength = 0;
		
		if(text == null || text.length()==0)
		{
			LOGGER.trace("request text is null or empty");
			return wordLength;
		}

		char ch[] = new char[text.length()];
		for (int i = 0; i < text.length(); i++)
		{
			ch[i] = text.charAt(i);
			if (((i > 0) && (ch[i] != ' ') && (ch[i - 1] == ' ')) || ((ch[0] != ' ') && (i == 0))) 
			{
				wordLength++;
			}
		}

		LOGGER.trace("response from getWordLength : " + wordLength);
		return wordLength;
	}

	/**
	 * 
	 * Helper method to determine postType
	 * 
	 * @param text
	 * @return
	 */
	public String getPostType(String text)
	{
		LOGGER.trace("in getPostType helper method... text : " + text);
		String postType = null;
		
		int wordLength = getWordLength(text);
		if(wordLength<=400)
		{
			LOGGER.trace("Text word length is less than or equal to 400 words");
			postType = PostConstants.POST_TYPE_QUICK_READ;
		}
		else if(wordLength>400 && wordLength<=2000)
		{
			LOGGER.trace("Text word length is less that 400 words");
			postType = PostConstants.POST_TYPE_WRITEUP;
		}
		else 
		{
			LOGGER.trace("Word length greater than specified limit.");
			postType = PostConstants.POST_TYPE_INVALID;
		}
		
		LOGGER.trace("Response from getPostType : " + postType);
		return postType;
	}

	/**
	 * 
	 * Helper method to create default hashtag depending on postTypes from request parameter
	 * 
	 * @author akshay
	 * @param postType
	 * @return List<String> defaultHashtags
	 */
	public List<String> getDefaultHashTags(String postType, List<String> hashtags)
	{
		LOGGER.trace("in getDefaultHashTags... postType : " + postType);
		List<String> defaultHashtags = new ArrayList<>();
	
		if(postType.equals(PostConstants.POST_TYPE_QUICK_READ))
		{
			LOGGER.trace("post type is : " + PostConstants.POST_TYPE_QUICK_READ);
			if(!hashtags.contains(PostConstants.DEFAULT_TAG_QUICK_READ))
			{
				defaultHashtags.add(PostConstants.DEFAULT_TAG_QUICK_READ);
			}
			if(!hashtags.contains(PostConstants.DEFAULT_TAG_USER_QUICK_READ))
			{
				defaultHashtags.add(PostConstants.DEFAULT_TAG_USER_QUICK_READ);
			}
				
		}
		else
		{
			LOGGER.trace("post type is : " + PostConstants.POST_TYPE_WRITEUP);
			if(!hashtags.contains(PostConstants.DEFAULT_TAG_WRITEUP))
			{
				defaultHashtags.add(PostConstants.DEFAULT_TAG_WRITEUP);
			}
			if(!hashtags.contains(PostConstants.DEFAULT_TAG_USER_WRITEUP))
			{
				defaultHashtags.add(PostConstants.DEFAULT_TAG_USER_WRITEUP);
			}
		}
		
		LOGGER.trace("Response from getDefaultHashTags : " + defaultHashtags);
		return defaultHashtags;
	}

	/**
	 * 
	 * Helper method to generate unique id for user post
	 * 
	 * @author akshay
	 * @return String postId
	 */
	public String generateUniquePostId()
	{
		LOGGER.trace("in generateUniquePostId");
		String uniquePostId = null;
		
		UUID uuid = UUID.randomUUID();
		uniquePostId = "post_" + uuid.toString();
		
		LOGGER.trace("Unique post id : " + uniquePostId);
		return uniquePostId;
	}
	
	/**
	 * 
	 * Helper method to generate unique commentId
	 * 
	 * @author akshay
	 * @param postType
	 * @return String uniqueCommentId
	 */
	public String generateUniqueCommentId(String postType)
	{
		LOGGER.trace("in generateUniqueCommentId : postType : " + postType);
		
		UUID uuid = UUID.randomUUID();
		
		String uniqueCommentId = PostConstants.COMMENT_ID_INITIAL + postType + "_" + uuid.toString();
		LOGGER.trace("Unique commentId : " + uniqueCommentId);
		
		LOGGER.trace("Response from generateUniqueCommentId... uniqueCommentId : " + uniqueCommentId);
		return uniqueCommentId;
	}

	/**
	 * 
	 * Helper method to get userId from requested token
	 * 
	 * @author akshay
	 * @param token
	 * @return
	 */
	public String getUserIdFromToken(String token)
	{
		LOGGER.trace("in getUserIdFromToken");
		UserDTO user = null;
		String userId = null;
		
		try 
		{
			user = userConsumer.getUserFromToken(token);
			if(user == null)
			{
				LOGGER.error("Server Error from component-user");
				userId = null;
			}
			
			userId = user.getId();
		}
		catch(Exception err) 
		{
			LOGGER.error("Error in getUserIdFromToken : " + err);
			user = null;
		}
		
		LOGGER.trace("Response from getUserIdFromToken : " + userId);
		return userId;
	}

	/**
	 * 
	 * Helper method determine if the requested text word length is under 200 words or not
	 * 
	 * @author akshay
	 * @param commentText
	 * @return boolean isCommentTextValid
	 */
	public boolean isCommentTextValid(String commentText)
	{
		LOGGER.trace("in isCommentTextValid... commentText : " + commentText);
		boolean isCommentTextValid = false;
		if(commentText.length()<=1000)
		{
			LOGGER.trace("comment text word length is below 1000. Valid");
			isCommentTextValid = true;
		}
		
		LOGGER.trace("Response from isCommentTextValid : " + isCommentTextValid);
		return isCommentTextValid;
	}


	/**
	 * 
	 * Helper method to validate if the character lenght of requested is in range according to requested postType
	 * 
	 * @author Akshay
	 * @param postType
	 * @param postType2
	 * @return
	 */
	public boolean isPostCharacterLenghtValid(String postText, String postType)
	{
		LOGGER.trace("in isPostCharacterLenghtValid helper method");
		boolean isPostCharacterLenghtValid = true;
		
		if(postType.equals(PostConstants.POST_TYPE_QUICK_READ) && postText.length()>= 1000)
		{
			LOGGER.trace("postType is quick-read and the character length excides 999 characters.");
			isPostCharacterLenghtValid = false;
		}
		else if(postType.equals(PostConstants.POST_TYPE_QUICK_READ) && postText.length()>= 15000)
		{
			LOGGER.trace("postType is writeup and the character length excides 14999 characters.");
			isPostCharacterLenghtValid = false;
		}
		
		LOGGER.trace("Response from isPostCharacterLenghtValid : " + isPostCharacterLenghtValid);
		return isPostCharacterLenghtValid;
	}


	/**
	 * 
	 * Helper method to calculate total character length of text from all the pages
	 * 
	 * @author Akshay
	 * @param pages
	 * @return
	 */
	public int getTotalCharacterCountForPagedPosts(List<PageDTO> pages)
	{
		LOGGER.trace("in getTotalCharacterCountForPagedPosts");
		int totalCharacterCount = 0;
		
		for(PageDTO eachPage : pages)
		{
			LOGGER.trace("Current page number : " + eachPage.getPageNumber());
			totalCharacterCount = totalCharacterCount + eachPage.getPageText().length();
		}
		
		LOGGER.trace("Response from getTotalCharacterCountForPagedPosts : " + totalCharacterCount);
		return totalCharacterCount;
	}

	/**
	 * 
	 * Helper method to verify is each pages have character count less than 2999 characters
	 * 
	 * @author Akshay
	 * @param pages
	 * @return
	 */
	public boolean isCharacterCountValidOnEachPage(List<PageDTO> pages)
	{
		LOGGER.trace("in isCharacterCountValidOnEachPage helper method");
		boolean isValid = true;
		
		for(PageDTO eachPage : pages)
		{
			LOGGER.trace("Current page : " + eachPage);
			if(eachPage.getPageText().length()>2999)
			{
				LOGGER.trace("Current page exceeds character length");
				isValid = false;
			}
		}
		
		LOGGER.trace("Response from isCharacterCountValidOnEachPage : " + isValid);
		return isValid;
	}


	
	/**
	 * 
	 * Helper method to validate requested hashtags
	 * 
	 * @author Akshay
	 * @param hashtags
	 * @return List of String of validated Hashtags
	 */
	public List<String> validateHashTags(List<String> hashtags)
	{
		LOGGER.trace("in validateHashTags helper method");
		List<String> validatedHashtags = new ArrayList<>();
		
		for(String eachHashtag : hashtags)
		{
			String validatedHashtag = new String();
			if(!eachHashtag.contains("#"))
			{
				validatedHashtag = "#" + eachHashtag;
			}
			else
			{
				validatedHashtag = eachHashtag;
			}
			validatedHashtags.add(validatedHashtag);
		}
		
		LOGGER.trace("Response from validateHashTags : " + validatedHashtags);
		return validatedHashtags;
	}


	/**
	 * 
	 * Service helper method to check if the requested data is older than 30 days or not
	 * 
	 * @author Akshay
	 * @param journalDate
	 * @return
	 * @throws ParseException 
	 */
    public boolean isDateOlderThan30Days(Date journalDate, Date currentDate) throws ParseException
    {
        LOGGER.trace("In isDateOlderThan30Days helper method... requestedDate : " + journalDate);
        boolean isOlder = true;

        try
        {
            long diff = currentDate.getTime() - journalDate.getTime();
            LOGGER.trace("Time difference : " + diff);
            
            long daysDifference = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            LOGGER.trace("Difference in time : " + daysDifference);

            if (daysDifference <= new Long(30))
            {
                LOGGER.trace("The difference of days between 2 dates is not more than 30 days");
                isOlder = false;
            }
        }
        catch (Exception err)
        {
            LOGGER.error("Error in isDateOlderThan30Days : " + err);
            return isOlder;
        }

        LOGGER.trace("Response from isDateOlderThan30Days : " + isOlder);
        return isOlder;
    }


    /**
     * 
     * Service helper method to get journalMonth string from journalDate
     * 
     * @author Akshay
     * @param journalDate
     * @return
     */
    public String getJournalMonth(Date journalDate)
    {
        LOGGER.trace("In getJournalMonth... journalDate : " + journalDate);
        String journalMonth = null;
        int i = 0;
        
        List<String> monthArr = new ArrayList<String>();
        monthArr.add("JAN");
        monthArr.add("FEB");
        monthArr.add("MAR");
        monthArr.add("APR");
        monthArr.add("MAY");
        monthArr.add("JUN");
        monthArr.add("JUL");
        monthArr.add("AUG");
        monthArr.add("SEPT");
        monthArr.add("OCT");
        monthArr.add("NOV");
        monthArr.add("DEC");
        
        while(i != journalDate.getMonth())
        {
            i++;
            if(i == journalDate.getMonth())
            {
                journalMonth = monthArr.get(i);
            }
        }
        
        LOGGER.trace("Response from getJournalMonth : " + journalMonth);
        return journalMonth;
    }


    /**
     * 
     * Service helper method to get journalYear string from journalDate
     * 
     * @author Akshay
     * @param journalDate
     * @return
     */
    public String getJournalYear(Date journalDate)
    {
        LOGGER.trace("in getJournalYear... journalDate : " + journalDate);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(journalDate);
        LOGGER.trace("formatted date : " + dateString);
        
        LOGGER.trace("Response from getJournalYear : " + dateString.substring(0, 4));
        return dateString.substring(0, 4);
    }

}
