package com.quot.api.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.comment.repository.CommentRepository;
import com.quot.api.constant.PostConstants;
import com.quot.api.dto.AddPostRequestWrapper;
import com.quot.api.dto.BulkUserResponse;
import com.quot.api.dto.CommentDTO;
import com.quot.api.dto.CommentEditWrapper;
import com.quot.api.dto.CommentRequestWrapper;
import com.quot.api.dto.CreateJournalWrapper;
import com.quot.api.dto.EditWriteUpRequestDTO;
import com.quot.api.dto.GetPostResponseWrapper;
import com.quot.api.dto.JournalDTO;
import com.quot.api.dto.JournalUpdateRequestDTO;
import com.quot.api.dto.PageDTO;
import com.quot.api.dto.PagedPostEditTextDTO;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.PostEditTextDTO;
import com.quot.api.dto.PostEditTitleDTO;
import com.quot.api.dto.Reply;
import com.quot.api.dto.ReplyCommentRequestWrapper;
import com.quot.api.dto.WriteUpComment;
import com.quot.api.dto.WriteUpDTO;
import com.quot.api.dto.WriteUpRequestDTO;
import com.quot.api.dto.WriteupResponseWrapper;
import com.quot.api.redis.timeline.service.UserHomeTimelineService;
import com.quot.api.redis.timeline.service.UserTimelineService;
import com.quot.api.repository.JournalRepository;
import com.quot.api.repository.PostRepository;
import com.quot.api.repository.WriteUpRespository;
import com.quot.api.service.helper.PostDateComparator;
import com.quot.api.service.helper.PostServiceHelper;
import com.quot.api.service.helper.StringHelper;
import com.quot.api.user.consumer.UserConsumer;
import com.quot.api.user.dto.UserDTO;

@Service
public class PostService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private JournalRepository journalRepository;

	@Autowired
	private WriteUpRespository writeupRepository;

	@Autowired
	private PostServiceHelper postServiceHelper;

	@Autowired
	private UserConsumer userConsumer;

	@Autowired
	private AuthConsumer authConsumer;

	@Autowired
	private StringHelper stringHelper;

	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private UserHomeTimelineService userHomeTimelineService;

	@Autowired
	private UserTimelineService userTimelineService;
	
	/**
	 * Service method to add a post
	 * 
	 * @param postRequest
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> addPostService(AddPostRequestWrapper addPostRequestWrapper, String token)
			throws Exception
	{
		LOGGER.trace("In addPostService method");
		Map<String, Object> responseMap = new HashMap<>();
		int postCharacterCount = 0;

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// For all the not paged posts
				if (!addPostRequestWrapper.isPaged())
				{
					if (addPostRequestWrapper.getText() == null || addPostRequestWrapper.getText() == "")
					{
						LOGGER.trace("Post text is empty or null. Bad Request");
						responseMap.put("statusCode", 400);
						responseMap.put("statusCode", "Bad Request");
						responseMap.put("message", "Post text is empty or null. Bad Request");
						return responseMap;
					}
					// Validate Post text character length against postType
					if (!postServiceHelper.isPostCharacterLenghtValid(addPostRequestWrapper.getText(),
							addPostRequestWrapper.getPostType()))
					{
						LOGGER.trace("Post text character length exceeded. Forbidden");
						responseMap.put("statusCode", 403);
						responseMap.put("statusMessage", "Forbidden");
						responseMap.put("message", "Post text character length exceeded. Forbidden");
						return responseMap;
					}
					
					postCharacterCount = addPostRequestWrapper.getText().length();
					LOGGER.trace("total character count for unpaged post : " + postCharacterCount);
				}
				// For all the paged posts
				else
				{
					// Check for postType
					if (addPostRequestWrapper.getPostType().equals(PostConstants.POST_TYPE_QUICK_READ))
					{
						LOGGER.trace("Quick-read post cannot be paged. Bad Request");
						responseMap.put("statusCode", 400);
						responseMap.put("statusCode", "Bad Request");
						responseMap.put("message", "Post text is empty or null. Bad Request");
						return responseMap;
					}

					// Check if pages are present or not
					if (addPostRequestWrapper.getPages() == null || addPostRequestWrapper.getPages().size() == 0)
					{
						LOGGER.trace("Empty pages. Bad Request");
						responseMap.put("statusCode", 400);
						responseMap.put("statusMessage", "Bad Request");
						responseMap.put("message", "Empty pages. Bad Request");
						return responseMap;
					}

					// Check if the characeter length for each page exceed desired character count
					// or not
					if (!postServiceHelper.isCharacterCountValidOnEachPage(addPostRequestWrapper.getPages()))
					{
						LOGGER.trace(
								"Post text character length on some pages exceed the desired character length. Bad Request");
						responseMap.put("statusCode", 400);
						responseMap.put("statusMessage", "Bad Request");
						responseMap.put("message",
								"Post text character length on some pages exceed the desired character length. Bad Request");
						return responseMap;
					}

					// Check if pages count exceed more than 5
					if (addPostRequestWrapper.getPages().size() > 5)
					{
						LOGGER.trace("Pages count exceeds more than 5. Bad Request");
						responseMap.put("statusCode", 400);
						responseMap.put("statusMessage", "Bad Request");
						responseMap.put("message", "Pages count exceeds more than 5. Bad Request");
						return responseMap;
					}
					
					for(PageDTO eachPage : addPostRequestWrapper.getPages())
					{
						postCharacterCount = postCharacterCount + eachPage.getPageText().length();	
					}
					
					LOGGER.trace("total character count for paged post : " + postCharacterCount);
					
				}
				
				// Validate hashtags
				List<String> validatedHashtags = postServiceHelper.validateHashTags(addPostRequestWrapper.getHashtags());
				LOGGER.trace("Validated hashtags : " + validatedHashtags);
				
				// Get user from token
				UserDTO user = userConsumer.getUserFromToken(token);
				if (user == null)
				{
					LOGGER.trace("User not found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("User : " + user.toString());
				}

				// Validate if the user requested for adding post has a profile or not
				if (!user.isProfilePresent())
				{
					LOGGER.trace("User profile not present. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "User profile not present. Precondition Failed");
					return responseMap;
				}

				String userId = user.getId();
				LOGGER.trace("userId : " + userId);

				// Add default hashtags
				List<String> defaultHashTags = postServiceHelper.getDefaultHashTags(addPostRequestWrapper.getPostType(),
						addPostRequestWrapper.getHashtags());
				LOGGER.trace("default hashtags associated to post : " + defaultHashTags);
				addPostRequestWrapper.getHashtags().addAll(defaultHashTags);

				// Create PostDTO
				Date currentDate = postServiceHelper.parseFormattedDate();
				String postId = postServiceHelper.generateUniquePostId();

				PostDTO userPost = new PostDTO();
				userPost.setHashtags(validatedHashtags);
				userPost.setUpvotes(new ArrayList<>());
				userPost.setAppreciates(new ArrayList<>());
				userPost.setComments(new ArrayList<>());
				userPost.setDraft(addPostRequestWrapper.isDraft());
				userPost.setDate(currentDate);
				userPost.setUpdatedDate(currentDate);
				userPost.setId(postId);
				userPost.setUserId(user.getId());
				userPost.setPostType(addPostRequestWrapper.getPostType());
				userPost.setPostTitle(addPostRequestWrapper.getTitle());
				userPost.setTotalCharacterCount(postCharacterCount);

				if (addPostRequestWrapper.isPaged())
				{
					userPost.setPaged(true);
					userPost.setPages(addPostRequestWrapper.getPages());
					userPost.setTotalCharacterCount(
							postServiceHelper.getTotalCharacterCountForPagedPosts(addPostRequestWrapper.getPages()));
					userPost.setPageCount(addPostRequestWrapper.getPages().size());
					userPost.setPostText(null);
				}
				else
				{
					userPost.setPaged(false);
					userPost.setPostText(addPostRequestWrapper.getText());
					userPost.setTotalCharacterCount(addPostRequestWrapper.getText().length());
					userPost.setPageCount(1);
					userPost.setPages(null);
				}

				// Save PostDTO
				LOGGER.trace("Saving post : " + userPost.toString());
				postRepository.save(userPost);
				
				if (!addPostRequestWrapper.isDraft())
				{
					LOGGER.trace("Requested post is not a draft");
					
					// Add post to user's followers in asyc manner
					userHomeTimelineService.fanoutToFollowers(userPost, token);

					// Add post to user's timeline in asyn manner
					userTimelineService.addToUserTimeline(userPost, token);
				}
				
				LOGGER.trace("User post posted successfully");
				responseMap.put("statusCode", 201);
				responseMap.put("statusMessage", "Created");
				responseMap.put("message", "User successfully posted the post");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in addPost service method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in addPost service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from addPost service" + responseMap);
		return responseMap;
	}

	/**
	 * Service method to delete current logged in user's post by input postId
	 * 
	 * @param postId
	 * @param token
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> deletePostService(String postId, String token) throws Exception
	{
		LOGGER.trace("in deletePostService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
			    LOGGER.trace("Authorized");
			    
			    UserDTO user = userConsumer.getUserFromToken(token);
			    LOGGER.trace("User data from the token : " + user.toString());
			    
				PostDTO post = postRepository.findById(postId);
				if (post == null)
				{
					LOGGER.trace("No post found associated to requested postId. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No post found associated to requested postId. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Current post : " + post.toString());
				}
				
				if(!user.getId().equals(post.getUserId()))
				{
				    LOGGER.trace("Current user is not allowed to delete the post. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Current user is not allowed to delete the post. Forbidden");
                    return responseMap;
				}

				List<String> commentIdListFromPost = post.getComments();
				LOGGER.trace("CommentId lists associated to user post : " + commentIdListFromPost);

				for (String eachCommentId : commentIdListFromPost)
				{
					LOGGER.trace("Current commentId : " + eachCommentId);
					commentRepository.deleteById(eachCommentId);
				}
				LOGGER.trace("All the comments associated to user post from comments data table");

				// Delete from all followers hometimeline
				userHomeTimelineService.deletePostEntryFromHomeTimeline(post, token);
				
				// Delete from user timeline
				userTimelineService.deletePostEntryFromUserTimeline(post, token);
				
				postRepository.deleteById(postId);
				LOGGER.trace("User post deleted successfully");
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "User post deleted successfully");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in deletePost service method : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in deletePostService service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from deletePost service" + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get all the posts of requested userId
	 * 
	 * @author akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUserPostsService(String userId, int offset, int limit, String token)
	{
		LOGGER.trace("in getUserPostsService... userId : " + userId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (userId == null || userId == "")
			{
				LOGGER.trace("Requested userId is null or empty. Bad Request");
				responseMap.put("statusCode", 400);
				responseMap.put("statusMessage", "Bad Request");
				responseMap.put("message", "Requested userId is null or empty. Bad Request");
				return responseMap;
			}

			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				Pageable pageable = PageRequest.of(offset, limit);
				Slice<PostDTO> userPostsSlice = postRepository.findByUserId(userId, pageable);
				if (userPostsSlice == null || userPostsSlice.getContent().size() == 0)
				{
					LOGGER.trace("No posts present for user");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No posts present for user");
					return responseMap;
				}
				else
				{
					LOGGER.trace("User posts : " + userPostsSlice.getContent());
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Ok");
					responseMap.put("posts", userPostsSlice.getContent());
				}
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUserPostsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserPostsService service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from getUserPostsService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to save draft to post
	 * 
	 * @author Akshay
	 * @param post
	 * @param token
	 * @return
	 */
	public Map<String, Object> saveDraftService(PostDTO post, String token)
	{
		LOGGER.trace("in saveDraftService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user data
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if(loggedInUser == null)
				{
					LOGGER.trace("Logged user data not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Logged user data not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user data : " + loggedInUser);
				}
				
				// Check if user has associated profile to account
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No profile associated to user account. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "No profile associated to user account. Precondition Failed");
					return responseMap;
				}
				
				// Check if the requested post is draft
				if(post.isDraft())
				{
					LOGGER.trace("Requested post is a draft. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Requested post is a draft. Conflict");
					return responseMap;
				}
				
				LOGGER.trace("Setting date and updated date");
				post.setDate(postServiceHelper.parseFormattedDate());
				post.setUpdatedDate(postServiceHelper.parseFormattedDate());
				
				// Saved post data
				postRepository.save(post);
				LOGGER.trace("Saved post data. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Saved post data. Successful");
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in saveDraftService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in saveDraftService service method : " + err);
			return responseMap;
		}
	
		LOGGER.trace("Response from saveDraftService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to get user saved drafts
	 * 
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUerDraftsService(String token)
	{
		LOGGER.trace("in getUerDraftsService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user from token
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if(loggedInUser == null)
				{
					LOGGER.trace("User data not found from token. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User data not found from token. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user data : " + loggedInUser);
				}
				
				// Check if user has associated profile
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("Logged in user has not profile associated to account. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "Logged in user has not profile associated to account. Precondition Failed");
					return responseMap;
				}
				
				// Get all the post which are saved as drafts
				List<PostDTO> userDrafts = postRepository.findUserPostDrafts(loggedInUser.getId());
				if(userDrafts == null || userDrafts.size() == 0)
				{
					LOGGER.trace("No user drafts saved. No Content");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "Logged in user has not profile associated to account. Precondition Failed");
					return responseMap;
				}
				
				// Sort the list according to date
				LOGGER.trace("Sorting user drafts list according to date");
				Collections.sort(userDrafts, new PostDateComparator());
				
				LOGGER.trace("User saved drafts : " + userDrafts);
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Successfully fetced user saved drafts. Successful");
				responseMap.put("drafts", userDrafts);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getUerDraftsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUerDraftsService service method : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getUerDraftsService : " + responseMap);
		return responseMap;
	}
	
	

	/**
	 * Service method to edit post's text comment
	 * 
	 * @author Akshay
	 * @param postEditDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> editPostTextService(PostEditTextDTO postEditDto, String token) throws Exception
	{
		LOGGER.trace("In editPost service... postEditDTO : " + postEditDto);
		Map<String, Object> responseMap = new HashMap<>();

		if (postEditDto.getNewText() == null || postEditDto.getNewText() == "" || postEditDto.getUserId() == null
				|| postEditDto.getUserId() == "" || postEditDto.getPostId() == null || postEditDto.getPostId() == "")
		{
			LOGGER.trace("Invalid inputs. Bad Request");
			responseMap.put("statusCode", 400);
			responseMap.put("statusMessage", "Bad Request");
			responseMap.put("message", "Invalid inputs. Bad Request");
			return responseMap;
		}

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				PostDTO tempPostDTO = postRepository.findById(postEditDto.getPostId());
				if (tempPostDTO == null)
				{
					LOGGER.trace("No post found for the given postId");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "Post not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("user post : " + tempPostDTO);
				}

				// Keeping existing copy for updating user and home timelines
				PostDTO presentPost = tempPostDTO;
				
				String userIdFromToken = postServiceHelper.getUserIdFromToken(token);
				if (userIdFromToken == null || userIdFromToken == "")
				{
					LOGGER.trace("Error from component-user. Server Error");
					responseMap.put("statusCode", 500);
					responseMap.put("statusMessage", "Server Error");
					responseMap.put("message", "Error from component-user. Server Error");
					return responseMap;
				}
				else
				{
					LOGGER.trace("userId from token : " + userIdFromToken);
				}

				if (!tempPostDTO.getUserId().equals(userIdFromToken))
				{
					LOGGER.trace("Current user is not allowed to edit the post. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Current user is not allowed to edit the post. Forbidden");
					return responseMap;
				}

				// Set post type
				String updatedPostType = postServiceHelper.getPostType(postEditDto.getNewText());
				LOGGER.trace("Updated post type : " + updatedPostType);

				tempPostDTO.setPostType(updatedPostType);
				tempPostDTO.setPostText(postEditDto.getNewText());
				tempPostDTO.setUpdatedDate(postServiceHelper.parseFormattedDate());
				LOGGER.trace("updated post data object : " + tempPostDTO);
				postRepository.save(tempPostDTO);
				
				// Update followers home timeline with updated post data
				userHomeTimelineService.updatePostOnHomeTimelines(presentPost, tempPostDTO, token);
				
				// Update user timeline with new data
				userTimelineService.updatePostOnUserTimeline(presentPost, tempPostDTO, token);
				
				LOGGER.trace("User post test edited successfully");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "User post test edited successfully");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in editPost service method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editPost service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from editPost service : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to edit text of particular page of a paged post
	 * 
	 * @author Akshay
	 * @param pagedPostEditTextDto
	 * @param token
	 * @return
	 */
	public Map<String, Object> editPagedPostTextService(PagedPostEditTextDTO pagedPostEditTextDto, String token)
	{
		LOGGER.trace("in editPagedPostTextService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Check if new text is empty or null
				if (stringHelper.isNullOrEmptyString(pagedPostEditTextDto.getNewText()))
				{
					LOGGER.trace("Updated page text is null/empty/. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Updated page text is null/empty/. Bad Request");
					return responseMap;
				}

				// Check if the new text character length exceeds desired length
				if (pagedPostEditTextDto.getNewText().length() > 2999)
				{
					LOGGER.trace("Post text character length exceed than desired. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message",
							"Post text character length on some pages exceed the desired character length. Bad Request");
					return responseMap;
				}

				PostDTO currentPost = postRepository.findById(pagedPostEditTextDto.getPostId());
				if (currentPost == null)
				{
					LOGGER.trace("Post associated to requested postId not found. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to requested postId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post associated to requested postId : " + currentPost.toString());
				}
				
				if(!currentPost.isPaged())
				{
					LOGGER.trace("The requested post has not pages available. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "The requested post has not pages available. Conflict");
					return responseMap;
				}

				if (!currentPost.getUserId().equals(pagedPostEditTextDto.getUserId()))
				{
					LOGGER.trace("User cannot perform the requested edit action. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "User cannot perform the requested edit action. Forbidden");
					return responseMap;
				}

				LOGGER.trace("Iterrating through pages");
				for(PageDTO eachPage : currentPost.getPages())
				{
					LOGGER.trace("Current page : " + eachPage.toString());
					if(eachPage.getPageNumber() == pagedPostEditTextDto.getPageNumber())
					{
						LOGGER.trace("Page number matched with requested page nummer");
						eachPage.setPageText(pagedPostEditTextDto.getNewText());
						eachPage.setPageTitle(pagedPostEditTextDto.getPageTitle());
					}
				}

				LOGGER.trace("Saving the updated data on posts data table");
				postRepository.save(currentPost);
				LOGGER.trace("Successfully updated required page text");
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Successfully updated required page text");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in editPagedPostTextService service method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editPagedPostTextService service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from editPagedPostTextService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to edit a title on user post
	 * 
	 * @param postEditTitleDto
	 * @param token
	 * @return
	 */
	public Map<String, Object> editPostTitleService(PostEditTitleDTO postEditTitleDto, String token)
	{
		LOGGER.trace("in editPostTitleService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				PostDTO tempPostDTO = postRepository.findById(postEditTitleDto.getPostId());
				if (tempPostDTO == null)
				{
					LOGGER.trace("No post found for the given postId");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "Post not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("user post : " + tempPostDTO);
				}

				String userIdFromToken = postServiceHelper.getUserIdFromToken(token);
				if (userIdFromToken == null || userIdFromToken == "")
				{
					LOGGER.trace("Error from component-user. Server Error");
					responseMap.put("statusCode", 500);
					responseMap.put("statusMessage", "Server Error");
					responseMap.put("message", "Error from component-user. Server Error");
					return responseMap;
				}
				else
				{
					LOGGER.trace("userId from token : " + userIdFromToken);
				}

				if (!tempPostDTO.getUserId().equals(userIdFromToken))
				{
					LOGGER.trace("Current user is not allowed to edit the post. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Current user is not allowed to edit the post. Forbidden");
					return responseMap;
				}

				// Set post type
				String updatedPostType = postServiceHelper.getPostType(postEditTitleDto.getNewTitle());
				LOGGER.trace("Updated post type : " + updatedPostType);

				tempPostDTO.setPostType(updatedPostType);
				tempPostDTO.setPostText(postEditTitleDto.getNewTitle());
				tempPostDTO.setUpdatedDate(postServiceHelper.parseFormattedDate());
				LOGGER.trace("updated post data object : " + tempPostDTO);
				postRepository.save(tempPostDTO);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in editPost service method : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		return null;
	}

	/**
	 * Service method to get Post via input profileId
	 * 
	 * @author Akshay
	 * @param postId
	 * @return
	 */
	public Map<String, Object> getPostByIdService(String postId, String token)
	{
		LOGGER.trace("In getPost Service method... input postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		PostDTO post = new PostDTO();
		GetPostResponseWrapper postResponseWrapper = new GetPostResponseWrapper();
		try
		{
			if (postId == null || postId == "")
			{
				LOGGER.trace("No postId present in the request. Bad Request");
				responseMap.put("message", "No postId present in the request. Bad Request");
				responseMap.put("statusMessage", "Bad Request");
				responseMap.put("statusCode", 400);
				return responseMap;
			}

			if (authConsumer.isTokenValid(token.substring(7)))
			{
			    UserDTO user = authConsumer.getCurrentUserFromToken(token);
			    LOGGER.trace("Logged in user : " + user.toString());
			    
				LOGGER.trace("Authorized");
				post = postRepository.findById(postId);
				LOGGER.trace("post from the postId : " + post);
				if (post == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("message", "user post associated to postId not found");
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("statusCode", 204);
					return responseMap;
				}
				else
				{
					LOGGER.trace("Requested post : " + post.toString());
				}
				
				List<CommentDTO> comments = new ArrayList<>();
				if(post.getComments() != null || post.getComments().size() > 0)
				{
				    LOGGER.trace("Fetching comments data for each comment");
				    for(String eachCommentId : post.getComments())
				    {
				        LOGGER.trace("Current commentId : " + eachCommentId);
				        CommentDTO comment = commentRepository.findById(eachCommentId);
				        if(comment !=null)
				        {
				            comments.add(comment);
				        }
				    }
				    
				    LOGGER.trace("List of comments data for the post : " + comments);
				}
				
				UserDTO postUser = userConsumer.getUserFromId(post.getUserId(), token);
				if(postUser == null)
				{
				    LOGGER.trace("Error from user component. Internal Server Error");
				    responseMap.put("message", "Error from user component. Internal Server Error");
                    responseMap.put("statusMessage", "Internal Server Error");
                    responseMap.put("statusCode", 500);
                    return responseMap;
				}
				else
				{
				    LOGGER.trace("User date associated userId : " + postUser.toString());
				}
				
				postResponseWrapper.setPost(post);
				postResponseWrapper.setUser(postUser);
				LOGGER.trace("Response wrapper : " + postResponseWrapper);

				responseMap.put("post", postResponseWrapper);
				responseMap.put("comments", comments);
				responseMap.put("message", "Successfully fetched the post data with comments. Successful");
				responseMap.put("statusMessage", "Successful");
				responseMap.put("statusCode", 200);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getPost service method : " + err);
			post = null;
			responseMap.put("statusMessage", "Server error");
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Error in getPost service method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from getPost service method : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to like a quote posted by user
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> upvotePostService(String postId, String token)
	{
		LOGGER.trace("in upvotePostService method... input postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		PostDTO tempPost = null;

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				// Get logged in user
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user data found from token. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "No user data found from token. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("logged in user data : " + loggedInUser.toString());
				}

				tempPost = postRepository.findById(postId);
				if (tempPost == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to postid " + postId + " not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post from the input postId : " + tempPost.toString());
				}

				// Checked if current user previousy liked the same post
				List<String> upvotes = tempPost.getUpvotes();
				LOGGER.trace("Upvotes list from post : " + upvotes);

				if (upvotes != null && upvotes.size() > 0 && upvotes.contains(loggedInUser.getId()))
				{
					LOGGER.trace("Post upvoted by current user : " + loggedInUser.getId());

					LOGGER.trace("Removing the upvote for avoiding the dublicate");
					tempPost.getUpvotes().remove(loggedInUser.getId());
				}

				// Check if same user has appreciated the same post
				List<String> appreciates = tempPost.getAppreciates();
				LOGGER.trace("Appreciates from the post : " + appreciates);
				if (appreciates.contains(loggedInUser.getId()))
				{
					LOGGER.trace("The user has appreciated the post previously");
					LOGGER.trace("Removing the appreciation");
					tempPost.getAppreciates().remove(loggedInUser.getId());
				}

				// Update the likes of the post
				tempPost.getUpvotes().add(loggedInUser.getId());
				LOGGER.trace("Updated post : " + tempPost);
				postRepository.save(tempPost);
				responseMap.put("message", "upvote action successful");
				responseMap.put("upvotes", tempPost.getUpvotes());
				responseMap.put("appreciates", tempPost.getAppreciates());
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in likePostServive method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in upvotePostService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from upvotePostService method : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get users data who upvoted request posts
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUpvoteUsersService(String postId, String token)
	{
		LOGGER.trace("in getUpvoteUsersService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				PostDTO post = postRepository.findById(postId);
				if (post == null)
				{
					LOGGER.trace("Post associated to requested postId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to requested postId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post associated to postId : " + postId);
				}

				List<String> upvotes = post.getUpvotes();
				LOGGER.trace("upvotes : " + upvotes);

				List<BulkUserResponse> bulkUsersData = userConsumer.getBulkUsers(upvotes, token);
				if (bulkUsersData == null || bulkUsersData.size() == 0)
				{
					LOGGER.trace("No users data found. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No users data found. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("bulk users data : " + bulkUsersData);
				}

				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("users", bulkUsersData);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUpvoteUsersService method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUpvoteUsersService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Reponse from getUpvoteUsersService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to remove user's upvote from a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> removeUpvotePostService(String postId, String token)
	{
		LOGGER.trace("in  removeUpvotePostService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		PostDTO tempPost = null;

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user data found from token. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "No user data found from token. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("logged in user data : " + loggedInUser.toString());
				}

				tempPost = postRepository.findById(postId);
				if (tempPost == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to postid " + postId + " not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post from the input postId : " + tempPost.toString());
				}

				List<String> upvotes = tempPost.getUpvotes();
				LOGGER.trace("upvotes list from post : " + upvotes);

				if (upvotes != null && upvotes.size() > 0 && upvotes.contains(loggedInUser.getId()))
				{
					LOGGER.trace("Post upvoted by current user : " + loggedInUser.getId());

					LOGGER.trace("Removing the upvote");
					tempPost.getUpvotes().remove(loggedInUser.getId());
					LOGGER.trace("Succesfully removed user upvote from the post");
					postRepository.save(tempPost);
				}

				responseMap.put("message", "Remove upvote action successful");
				responseMap.put("upvotes", tempPost.getUpvotes());
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in removeUpvotePostService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in removeUpvotePostService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from removeUpvotePostService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to like a quote posted by user
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> appreciatePostService(String postId, String token)
	{
		LOGGER.trace("in upvotePostService method... input postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		PostDTO tempPost = null;

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				// Get logged in user
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user data found from token. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "No user data found from token. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("logged in user data : " + loggedInUser.toString());
				}

				tempPost = postRepository.findById(postId);
				if (tempPost == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to postid " + postId + " not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post from the input postId : " + tempPost.toString());
				}

				// Checked if current user previousy liked the same post
				List<String> appreciates = tempPost.getAppreciates();
				LOGGER.trace("Appreciates list from post : " + appreciates);

				if (appreciates != null && appreciates.size() > 0 && appreciates.contains(loggedInUser.getId()))
				{
					LOGGER.trace("Post appreciated by current user : " + loggedInUser.getId());

					LOGGER.trace("Removing the appreciate for avoiding the dublicate");
					tempPost.getAppreciates().remove(loggedInUser.getId());
				}

				// Check if same user has appreciated the same post
				List<String> upvotes = tempPost.getUpvotes();
				LOGGER.trace("Upvotes from the post : " + appreciates);
				if (upvotes.contains(loggedInUser.getId()))
				{
					LOGGER.trace("The user has appreciated the post previously");
					LOGGER.trace("Removing the appreciation");
					tempPost.getUpvotes().remove(loggedInUser.getId());
				}

				// Update the appreicates of the post
				tempPost.getAppreciates().add(loggedInUser.getId());
				LOGGER.trace("Updated post : " + tempPost);
				postRepository.save(tempPost);
				responseMap.put("upvotes", tempPost.getUpvotes());
				responseMap.put("appreciates", tempPost.getAppreciates());
				responseMap.put("message", "appreciate action successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in likePostServive method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in upvotePostService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from upvotePostService method : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get users data who appreciated the post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getAppreciateUsersService(String postId, String token)
	{
		LOGGER.trace("in getAppreciateUsersService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				PostDTO post = postRepository.findById(postId);
				if (post == null)
				{
					LOGGER.trace("Post associated to requested postId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to requested postId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post associated to postId : " + postId);
				}

				List<String> appreciates = post.getAppreciates();
				LOGGER.trace("upvotes : " + appreciates);

				List<BulkUserResponse> bulkUsersData = userConsumer.getBulkUsers(appreciates, token);
				if (bulkUsersData == null || bulkUsersData.size() == 0)
				{
					LOGGER.trace("No users data found. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No users data found. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("bulk users data : " + bulkUsersData);
				}

				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("users", bulkUsersData);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getAppreciateUsersService method : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getAppreciateUsersService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Reponse from getAppreciateUsersService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to remove user's upvote from a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> removeAppreciatePostService(String postId, String token)
	{
		LOGGER.trace("in  removeAppreciatePostService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		PostDTO tempPost = null;

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user data found from token. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "No user data found from token. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("logged in user data : " + loggedInUser.toString());
				}

				tempPost = postRepository.findById(postId);
				if (tempPost == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to postid " + postId + " not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post from the input postId : " + tempPost.toString());
				}

				List<String> appreciates = tempPost.getAppreciates();
				LOGGER.trace("Appreciate list from post : " + appreciates);

				if (appreciates != null && appreciates.size() > 0 && appreciates.contains(loggedInUser.getId()))
				{
					LOGGER.trace("Post appreciated by current user : " + loggedInUser.getId());

					LOGGER.trace("Removing the appreciate");
					tempPost.getAppreciates().remove(loggedInUser.getId());
					LOGGER.trace("Succesfully removed user appreciation from the post");
					postRepository.save(tempPost);
				}

				responseMap.put("appreciates", tempPost.getAppreciates());
				responseMap.put("message", "Remove appreciate action successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in removeAppreciatePostService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in removeUpvotePostService method : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from removeAppreciatePostService : " + responseMap);
		return responseMap;
	}

	/**
	 * Service method to add comment on a post
	 * 
	 * @author Akshay
	 * @param commentRequestDTO
	 * @param token
	 * @return
	 */
	public Map<String, Object> commentService(CommentRequestWrapper commentRequestWrapper, String token)
	{
		LOGGER.trace("In commentService... input commentRequestDto : " + commentRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// 1 - Check if comment text is valid or not
				if (!postServiceHelper.isCommentTextValid(commentRequestWrapper.getCommentText()))
				{
					LOGGER.trace("Comment text word lenght greater than 200 words. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Comment text word lenght greater than 200 words. Forbidden");
					return responseMap;
				}

				// 2 - Get logged in user information from token
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// 3 - Get post data associated to requested postId
				String postId = commentRequestWrapper.getPostId();
				LOGGER.trace("Requested postId : " + postId);
				PostDTO tempPost = postRepository.findById(postId);
				if (tempPost == null)
				{
					LOGGER.trace("Post not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated with postId : " + postId + " not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("post from database : " + tempPost.toString());
				}

				if(tempPost.getPostType() == null || tempPost.getPostType()=="")
				{
				    LOGGER.trace("Invalid post type. Conflict");
                    responseMap.put("statusCode", 409);
                    responseMap.put("statusMessage", "Conflict");
                    responseMap.put("message", "Invalid post type. Conflict");
                    return responseMap;
				}
				
				String userId = loggedInUser.getId();
				LOGGER.trace("userId : " + userId);

				// 4 - Generate unique id from comment
				String commentId = postServiceHelper.generateUniqueCommentId(tempPost.getPostType());
				LOGGER.trace("unique commentId : " + commentId);

				// 5 - Create comment data object and save
				Date date = postServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + date);
				CommentDTO comment = new CommentDTO();
				comment.setId(commentId);
				comment.setEdited(false);
				comment.setDate(date);
				comment.setReply(new Reply());
				comment.setLikes(new ArrayList<>());
				comment.setUnlikes(new ArrayList<>());
				comment.setReports(new ArrayList<>());
				comment.setUserId(userId);
				comment.setCommentText(commentRequestWrapper.getCommentText());
				LOGGER.trace("Comment data : " + comment.toString());
				commentRepository.save(comment);

				// 6 - Associate comment object to request post data
				tempPost.getComments().add(comment.getId());
				LOGGER.trace("Added user comment to post : " + tempPost.toString());

				// 7 - Save updated post data to database
				postRepository.save(tempPost);

				responseMap.put("comment", comment);
				responseMap.put("statusCode", 200);
				responseMap.put("message", "Successful");
				responseMap.put("message", "Comment successfully added to user post. Succesful");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in commentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in commentService service : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from commentService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to get all the comments from a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getAllCommentFromAPostService(String postId, String token)
	{
		LOGGER.trace("in getAllCommentFromAPostService");
		Map<String, Object> responseMap = new HashMap<>();
		List<CommentDTO> comments = new ArrayList<>();
		
		try 
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// Get post data

				PostDTO post = postRepository.findById(postId);
				if (post == null)
				{
					LOGGER.trace("Requested post associated to postId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Requested post associated to postId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Requested post data : " + post.toString());
				}

				if (post.getComments() == null || post.getComments().size() == 0)
				{
					LOGGER.trace("There are not comments on the post. Success");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successfull");
					responseMap.put("message", "There are not comments on the post. Success");
					responseMap.put("comments", comments);
					return responseMap;
				}

				List<String> commentIdsList = post.getComments();
				for (String eachCommentId : commentIdsList)
				{
					LOGGER.trace("Current commentId string : " + eachCommentId);
					CommentDTO comment = getCommentById(eachCommentId);
					if (comment != null)
					{
						LOGGER.trace("Current comment data : " + comment.toString());
						comments.add(comment);
					}
				}
				LOGGER.trace("Comments associated to the post : " + comments);

				if (comments.size() == 0)
				{
					LOGGER.trace("No comments data found. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "No comments data found. Conflict");
					return responseMap;
				}
				
				LOGGER.trace("Comments data successfully fetced. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Success");
				responseMap.put("message", "Comments data successfully fetced. Successful");
				responseMap.put("comments", comments);

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getAllCommentFromAPostService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getAllCommentFromAPostService service : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getAllCommentFromAPostService : " + responseMap);
		return responseMap;
	}
	
	

	/**
	 * 
	 * Service method to get comment data by commentId
	 * 
	 * @author Akshay
	 * @param commentId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getCommentByIdService(String commentId, String token)
	{
		LOGGER.trace("in getCommentByIdService... commentId : " + commentId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				CommentDTO comment = getCommentById(commentId);
				if(comment == null)
				{
					LOGGER.trace("No comment associated to commentId found. Not Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No comment associated to commentId found. Not Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Comment data : " + comment.toString());
				}
				
				LOGGER.trace("Comment data successfully fetched. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Comment data successfully fetched. Successful");
				responseMap.put("comment", comment);
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getCommentByIdService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getCommentByIdService service : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getCommentByIdService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to get comment data from requested commentId
	 * 
	 * @author Akshay
	 * @param commentId
	 * @return
	 */
	public CommentDTO getCommentById(String commentId)
	{
		LOGGER.trace("in getCommentById");
		CommentDTO comment = commentRepository.findById(commentId);
		LOGGER.trace("Comment data : " + comment);
		return comment;
	}
	

	/**
	 * 
	 * Service method to edit a comment
	 * 
	 * @author Akshay
	 * @param editCommentDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> editCommentService(CommentEditWrapper commentEditWrapper, String token) throws Exception
	{
		LOGGER.trace("In editCommentService method... commentEditWrapper: " + commentEditWrapper);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				if (!postServiceHelper.isCommentTextValid(commentEditWrapper.getNewCommentText()))
				{
					LOGGER.trace("Comment text word lenght greater than 200 words. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Comment text word lenght greater than 200 words. Forbidden");
					return responseMap;
				}

				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				PostDTO post = postRepository.findById(commentEditWrapper.getPostId());
				if (post == null)
				{
					LOGGER.trace(
							"No post found associated to postId " + commentEditWrapper.getPostId() + ". Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "Not Found");
					responseMap.put("message",
							"No post found associated to postId " + commentEditWrapper.getPostId() + ". Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post data : " + post.toString());
				}

				Optional<String> requestedOptionalCommentId = post.getComments().stream()
						.filter(eachComment -> eachComment.equals(commentEditWrapper.getCommentId())).findFirst();
				if (requestedOptionalCommentId == null)
				{
					LOGGER.trace("Comment associated to requested commentId not found for requested user post");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "Not Found");
					responseMap.put("message",
							"Comment associated to requested commentId not found for requested user post");
					return responseMap;
				}

				CommentDTO requiredComment = commentRepository.findById(requestedOptionalCommentId.get());
				LOGGER.trace("Required comment : " + requiredComment.toString());

				if (requiredComment.getUserId() != loggedInUser.getId())
				{
					LOGGER.trace("User not allowed to perform requested action. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "User not allowed to perform requested action. Forbidden");
					return responseMap;
				}

				String commentId = requiredComment.getId();
				LOGGER.trace("commentId : " + commentId);
				post.getComments().remove(commentId);
				LOGGER.trace("Removed old comment data from user post");

				CommentDTO updatedComment = requiredComment;
				updatedComment.setCommentText(commentEditWrapper.getNewCommentText());
				updatedComment.setEdited(true);
				updatedComment.setUpdatedDate(postServiceHelper.parseFormattedDate());
				LOGGER.trace("Updated comment data : " + updatedComment);
				commentRepository.save(updatedComment);

				post.getComments().add(updatedComment.getId());
				postRepository.save(post);
				LOGGER.trace("Saved user post with updated comment");

				responseMap.put("statusCode", 200);
				responseMap.put("message", "Successful");
				responseMap.put("message", "Comment successfully edited for requested user post");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in editCommentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editCommentService : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from editCommentService method : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to delete a comment from a post This comment can only be
	 * deleted either by the user who quoted the post or the user who commented on
	 * the post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param commentId
	 * @param userName
	 * @param token
	 * @return
	 */
	public Map<String, Object> deleteCommentService(String postId, String commentId, String token)
	{
		LOGGER.trace("in deleteCommentService method... input postId : " + postId + ", commentId : " + commentId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				UserDTO user = userConsumer.getUserFromToken(token);
				if (user == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + user.toString());
				}

				// Check if the post exists
				PostDTO post = postRepository.findById(postId);
				if (post == null)
				{
					LOGGER.trace("No Post found associated to postId : " + postId);
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No Post found associated to postId : " + postId);
					return responseMap;
				}
				else
				{
					LOGGER.trace("Post from the postId : " + post);
				}

				List<String> commentsList = post.getComments();
				LOGGER.trace("commentList : " + commentsList);

				// Check of comment is present
				Optional<String> requiredCommentOptional = commentsList.stream()
						.filter(eachComment -> commentId.equals(eachComment)).findFirst();
				if (requiredCommentOptional == null)
				{
					LOGGER.trace("Comment marked for deletion not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Comment associated to commentId " + commentId + " not found");
					return responseMap;
				}

				// Check of the user is authorized to delete the comment
				CommentDTO requiredComment = commentRepository.findById(requiredCommentOptional.get());
				LOGGER.trace("Comment to be deleted : " + requiredComment);
				if (!post.getUserId().equals(user.getId()) && !requiredComment.getUserId().equals(user.getId()))
				{
					LOGGER.trace("User is not authorized to perform requested action");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "User not allowed to perform requested action. Forbidden");
					return responseMap;

				}

				commentRepository.deleteById(commentId);
				LOGGER.trace("Deleted comment data from comment data table");

				commentsList.remove(requiredComment.getId());
				LOGGER.trace("Required comment delete from comments list : " + commentsList);
				post.setComments(commentsList);
				LOGGER.trace("Updated the latest comments list to post data : " + post.toString());
				postRepository.save(post);
				LOGGER.trace("Saved updated post data");

				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Comment successfully deleted for requested user post");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in deleteCommentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in deleteCommentService : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from deleteCommentService method : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method add reply to a comment
	 * 
	 * @param replyCommentRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> replyCommentService(ReplyCommentRequestWrapper replyCommentRequestWrapper, String token)
	{
		LOGGER.trace("in replyCommentService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// 1 - Validate reply text character length
				if (!postServiceHelper.isCommentTextValid(replyCommentRequestWrapper.getReplyText()))
				{
					LOGGER.trace("Reply character lenght exceeds 1000 characters. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Reply character lenght exceeds 1000 characters. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Reply character is valid");
				}

				// 2 - Get logged in user information from token
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// 3 - Get the comment from requested commentId
				CommentDTO comment = commentRepository.findById(replyCommentRequestWrapper.getCommentId());
				if (comment == null)
				{
					LOGGER.trace("Comment associated to the commentId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Comment associated to the commentId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Comment : " + comment.toString());
				}

				// 4 - Check of comment already has a reply
				if (comment.getReply().getUserId()!=null)
				{
					LOGGER.trace("Comment already has reply. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Comment already has reply. Forbidden");
					return responseMap;
				}

				// 5 - Create a reply data object
				Date currentDate = postServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + currentDate.toString());

				Reply reply = new Reply();
				reply.setReplyText(replyCommentRequestWrapper.getReplyText());
				reply.setUserId(loggedInUser.getId());
				reply.setUsername(loggedInUser.getUsername());
				reply.setName(loggedInUser.getName());
				reply.setAvatar(loggedInUser.getAvatar());
				reply.setDate(currentDate);
				reply.setUpdatedDate(currentDate);
				LOGGER.trace("Reply data object : " + reply.toString());

				comment.setReply(reply);
				commentRepository.save(comment);
				LOGGER.trace("Comment data object updated and saved to data table. Successful");

				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Comment data object updated and saved to data table. Successful");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in commentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in commentService service : " + err);
			return responseMap;
		}

		LOGGER.trace("response from replyCommentService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method od edit reply from a comment
	 * 
	 * @author Akshay
	 * @param replyCommentRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> editReplyCommentService(
			ReplyCommentRequestWrapper replyCommentRequestWrapper, String token
	)
	{
		LOGGER.trace("in editReplyCommentService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// 1 - Validate reply text character length
				if (!postServiceHelper.isCommentTextValid(replyCommentRequestWrapper.getReplyText()))
				{
					LOGGER.trace("Reply character lenght exceeds 1000 characters. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Reply character lenght exceeds 1000 characters. Bad Request");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Reply character is valid");
				}

				// 2 - Get logged in user information from token
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// 3 - Get the comment from requested commentId
				CommentDTO comment = commentRepository.findById(replyCommentRequestWrapper.getCommentId());
				if (comment == null)
				{
					LOGGER.trace("Comment associated to the commentId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Comment associated to the commentId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Comment : " + comment.toString());
				}

				// 4 - Check of comment already has a reply
				if (comment.getReply() == null)
				{
					LOGGER.trace("Comment has no has reply. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Comment already has reply. Forbidden");
					return responseMap;
				}

				// 5 - Check if user is allowed to edit comment text or not
				if (!comment.getReply().getUserId().equals(loggedInUser.getId()))
				{
					LOGGER.trace("The logged in user has not added the reply and cannot edit. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The logged in user has not added the reply and cannot edit. Forbidden");
					return responseMap;
				}

				// 5 - Create a reply data object
				Date currentDate = postServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + currentDate.toString());

				Reply reply = comment.getReply();
				reply.setReplyText(replyCommentRequestWrapper.getReplyText());
				reply.setUpdatedDate(currentDate);
				LOGGER.trace("Reply data object : " + reply.toString());
				comment.setReply(reply);
				commentRepository.save(comment);
				LOGGER.trace("Comment data object updated and saved to data table. Successful");

				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message",
						"Comment data object updated and saved new reply text to data table. Successful");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in commentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in commentService service : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from editReplyCommentService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to delete a reply from comment
	 * 
	 * @param commentId
	 * @param token
	 * @return
	 */
	public Map<String, Object> deleteReplyCommentService(String commentId, String token)
	{
		LOGGER.trace("in deleteReplyCommentService... commentId : " + commentId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if (loggedInUser == null)
				{
					LOGGER.trace("Logged in user data not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged in user data not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				CommentDTO comment = commentRepository.findById(commentId);
				if (comment == null)
				{
					LOGGER.trace("Comment associated to the commentId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Comment associated to the commentId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Comment : " + comment.toString());
				}

				if (!loggedInUser.getId().equals(comment.getReply().getUserId()))
				{
					LOGGER.trace("Loggedin user is not the one who replied to comment hence cannot edit. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Loggedin user is not the one who replied to comment hence cannot edit. Forbidden");
					return responseMap;
				}

				comment.setReply(new Reply());
				commentRepository.save(comment);
				LOGGER.trace("Deleted reply from comment");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Reply successfully deleted from the comment data object. Successful");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in commentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in commentService service : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from deleteReplyCommentService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to get count of likes and comments from the requested postId
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getLikesAndCommentsCountService(String postId, String token)
	{
		LOGGER.trace("in getLikesAndCommentsCountService... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				UserDTO loggedInUser = userConsumer.getUserFromToken(token);
				if(loggedInUser == null)
				{
					LOGGER.trace("Logged user date not found. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Logged user date not found. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Loggedin user data : " + loggedInUser.toString());
				}
				
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("Loggedin user does not have a profile associated to account. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Loggedin user does not have a profile associated to account. Conflict");
					return responseMap;
				}
				
				PostDTO requestedPost = postRepository.findById(postId);
				if(requestedPost == null)
				{
					LOGGER.trace("Requested post is not present. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Requested post is not present. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Requested post data : " + requestedPost.toString());
				}
				
				int comments = 0;
				int upvotes = 0;
				int appreciates = 0;

				if (requestedPost.getComments() != null)
				{
					comments = requestedPost.getComments().size();
				}
				if (requestedPost.getUpvotes() != null)
				{
					upvotes = requestedPost.getUpvotes().size();
				}
				if (requestedPost.getAppreciates() != null)
				{
					appreciates = requestedPost.getAppreciates().size();
				}
				
				LOGGER.trace("counts for upvotes : " + upvotes + ", appreciates : " + appreciates + ", comments : " + comments);
				
				responseMap.put("upvotes", upvotes);
				responseMap.put("appreicates", appreciates);
				responseMap.put("comments", comments);
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform the requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getLikesAndCommentsCountService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getLikesAndCommentsCountService service : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getLikesAndCommentsCountService : " + responseMap);
		return responseMap;
	}

	/**
	 * Service method to get all the posts present on database
	 * 
	 * @author Akshay
	 * @return
	 */
//	public PostsResponseWrapper getAllPostServices(String username, String token) throws Exception
//	{
//		LOGGER.trace("In GetAllPost service method");
//		PostsResponseWrapper responseWrapper = new PostsResponseWrapper();
//		List<PostDTO> currentUserPostsList = new ArrayList<>();
//		List<PostDTO> postsList = new ArrayList<>();
//		ProfileDTO currentUserProfile = null;
//		List<String> currentUserFollowersList = new ArrayList<>();
//
//		try
//		{
//			if (authConsumer.isTokenValid(token.substring(7)))
//			{
//
////				if(filter==null || filter == "")
////				{
//				LOGGER.trace("input filter not applied");
//				currentUserProfile = userProfileConsumer.getUserProfile(username, token);
//				LOGGER.trace("Current user profile : " + currentUserProfile);
//
//				// Get current user posts
//				// currentUserPostsList = postRepository.findByUsername(username);
//				LOGGER.trace("current user posts : " + currentUserPostsList);
//				postsList.addAll(currentUserPostsList);
//
//				if (currentUserProfile == null)
//				{
//					LOGGER.trace("No profile present for the user");
//					responseWrapper.setStatusCode(204);
//					return responseWrapper;
//				}
//
//				currentUserFollowersList = currentUserProfile.getFollowing();
//				LOGGER.trace("current user followings : " + currentUserProfile.getFollowing());
//				LOGGER.trace("current user followings : " + currentUserFollowersList);
//				if (currentUserFollowersList == null || currentUserFollowersList.size() == 0)
//				{
//					LOGGER.trace("there are no followers for the current user");
//					responseWrapper.setStatusCode(200);
//					responseWrapper.setPosts(new ArrayList<>());
//					return responseWrapper;
//				}
//
//				for (String eachFollower : currentUserFollowersList)
//				{
//					LOGGER.trace("current follower : " + eachFollower);
//					// postsList.addAll(postRepository.findByUsername(eachFollower));
//				}
//
////				}
////				else
////				{
////					LOGGER.trace("input filter is applied : " + filter);
////				}
//
//				LOGGER.trace("postList : " + postsList);
//				if (postsList.size() == 0)
//				{
//					LOGGER.trace("No posts found");
//					responseWrapper.setPosts(postsList);
//					responseWrapper.setStatusCode(204);
//					return responseWrapper;
//				}
//
//				responseWrapper.setStatusCode(200);
//				responseWrapper.setPosts(postsList);
//			}
//			else
//			{
//				LOGGER.trace("Unauthorized");
//				responseWrapper.setPosts(postsList);
//				responseWrapper.setStatusCode(403);
//			}
//		}
//		catch (Exception err)
//		{
//			LOGGER.trace("Error in getAllPosts service method : " + err.getMessage());
//			responseWrapper.setStatusCode(500);
//		}
//
//		LOGGER.trace("Response from getAllPosts service method : " + responseWrapper);
//		return responseWrapper;
//	}

	/**
	 * 
	 * Service method to get all posts of current user
	 * 
	 * @author Akshay
	 * @param userName
	 * @return
	 */
	public Map<String, Object> getCurrentUserPosts(String userName, String token)
	{
		LOGGER.trace("In getCurrentUserPosts service method... userName: " + userName);
		Map<String, Object> responseMap = new HashMap<>();
		List<PostDTO> postList = new ArrayList<>();

		try
		{
			// Check if current username is current logged in user
			String currentLoggedUsername = authConsumer.getUsernameFromToken(token.substring(7));
			LOGGER.trace("Current userName from token : " + currentLoggedUsername);
			if (!currentLoggedUsername.equals(userName))
			{
				LOGGER.trace("logged in user and input userName are not same.");
				responseMap.put("statusCode", 409);
				responseMap.put("message", "Conflict");
				return responseMap;
			}

			// postList = postRepository.findByUsername(userName);
			LOGGER.trace("postList : " + postList);
			if (postList == null || postList.size() == 0)
			{
				LOGGER.trace("User has not quoted posts");
				responseMap.put("data", new ArrayList<>());
				responseMap.put("statusCode", 204);
				responseMap.put("message", "No posts present");
				return responseMap;
			}

			responseMap.put("data", postList);
			responseMap.put("statusCode", 200);
			responseMap.put("message", "Successful");

		}
		catch (Exception err)
		{
			LOGGER.error("Error in getCurrentUserPost service : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from getCurrentUserPost service method : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to create user journal
	 * 
	 * @author Akshay
	 * @param createJournalWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> createJournalService(CreateJournalWrapper createJournalWrapper, String token) throws Exception
	{
		LOGGER.trace("In createJournal service method... input data : " + createJournalWrapper.toString());
		Map<String, Object> responseMap = new HashMap<>();

		if (authConsumer.isTokenValid(token.substring(7)))
		{
			try
			{
			    // Check the length of content in journalContent
			    if(createJournalWrapper.getJournalContent().length() > 14999)
			    {
			        LOGGER.trace("Content length exceeds 14999 characters. Bad Request");
			        responseMap.put("statusCode", 400);
                    responseMap.put("statusMessage", "Bad Request");
                    responseMap.put("message", "Content length exceeds 14999 characters. Bad Request");
                    return responseMap;
			    }
			    
			    // Check if any journal present on the same journalDate
                if (null != journalRepository.findByJournalDate(createJournalWrapper.getJournalDate()))
                {
                    LOGGER.trace("A journal already present on the requested Journal date. Bad Request");
                    responseMap.put("statusCode", 400);
                    responseMap.put("statusMessage", "Bad Request");
                    responseMap.put("message", "A journal already present on the requested Journal date. Bad Request");
                    return responseMap;
                }
			    
				// Get logged in user
				UserDTO userFromToken = authConsumer.getCurrentUserFromToken(token);
				if(userFromToken == null)
				{
				    LOGGER.trace("Logged in user data not found. Forbidden");
				    responseMap.put("statusCode", 403);
	                responseMap.put("statusMessage", "Forbidden");
	                responseMap.put("message", "Logged in user data not found. Forbidden");
	                return responseMap;
				}
				else
				{
				    LOGGER.trace("logged in user : " + userFromToken.toString());
				}
				
				// Validate hashtags
                List<String> validatedHashtags = postServiceHelper.validateHashTags(createJournalWrapper.getHashtags());
                LOGGER.trace("Validated hashtags : " + validatedHashtags);
				
				Date currentDate = postServiceHelper.parseFormattedDate();
		        LOGGER.trace("Current date : " + currentDate);
				
				// Validate the journalDate not earlier than 30 days from the current date
				if(postServiceHelper.isDateOlderThan30Days(createJournalWrapper.getJournalDate(), currentDate))
				{
				    LOGGER.trace("The difference of days between 2 dates is more than 30 days. Not Acceptable");
				    responseMap.put("statusCode", 406);
                    responseMap.put("statusMessage", "Not Acceptable");
                    responseMap.put("message", "The differsence of days between 2 dates is more than 30 days. Not Acceptable");
                    return responseMap;
				}
				
				UUID uuid = UUID.randomUUID();
				String journalId = "journal_" + uuid.toString();
				LOGGER.trace("Generated unique journalId : " + journalId);
				
				String journalMonth = postServiceHelper.getJournalMonth(createJournalWrapper.getJournalDate());
				LOGGER.trace("Journal month : " + journalMonth);
				
				String journalYear = postServiceHelper.getJournalYear(createJournalWrapper.getJournalDate());
				LOGGER.trace("Journal year : " + journalYear);
				
				JournalDTO journal = new JournalDTO();
				journal.setId(journalId);
				journal.setUserId(userFromToken.getId());
				journal.setIsPosted(createJournalWrapper.isShouldPost());
				if(createJournalWrapper.getJournalTitle() !=null || createJournalWrapper.getJournalTitle() !="")
				{
				    journal.setJournalTitle(createJournalWrapper.getJournalTitle());
				}
				else
				{
				    journal.setJournalTitle("");
				}
				journal.setJournalContent(createJournalWrapper.getJournalContent());
				journal.setHashtags(validatedHashtags);
				journal.setJournalDate(createJournalWrapper.getJournalDate());
				journal.setJournalMonth(journalMonth);
				journal.setJournalYear(journalYear);
				journal.setDate(currentDate);
				journal.setUpdatedDate(currentDate);
				LOGGER.trace("Journal data object : " + journal.toString());
				journalRepository.save(journal);
				LOGGER.trace("Journal data object saved to database");
				
				if(createJournalWrapper.isShouldPost())
				{
				    // TO-DO create a post with journal data and call addPostMethod
				    
				    LOGGER.trace("Added user journal data to database and posted as user-journal. Created");
				    responseMap.put("message", "Added user journal data to database and posted as user-journal. Created");
				}
				else
				{
				    LOGGER.trace("Added user journal data to database. Created");
				    responseMap.put("message", "Added user journal data to database. Created");
				}
				
				responseMap.put("statusCode", 201);
				responseMap.put("statusMessage", "Created");

			}
			catch (Exception err)
			{
				LOGGER.error("Error in createJorunal service : " + err);
				responseMap.put("statusCode", 500);
				responseMap.put("statusMessage", "Server error");
				responseMap.put("message", "Error in createJorunal service : " + err);
				return responseMap;
			}
		}
		else
		{
		    LOGGER.trace("The user is not authroized to perform requested action. Unauthorized");
			responseMap.put("statusCode", 401);
			responseMap.put("statusMessage", "Unauthorized");
			responseMap.put("message", "The user is not authroized to perform requested action. Unauthorized");
			return responseMap;
		}

		LOGGER.trace("Response from createJournal service : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to delete a user journal
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> deleteJournalService(String journalId, String token)
			throws Exception
	{
		LOGGER.trace("In deleteJournalService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
		    if(authConsumer.isTokenValid(token.substring(7)))
		    {
		        LOGGER.trace("Authorized");
		        
		        // Get logged in user
                UserDTO userFromToken = authConsumer.getCurrentUserFromToken(token);
                if(userFromToken == null)
                {
                    LOGGER.trace("Logged in user data not found. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user data not found. Forbidden");
                    return responseMap;
                }
                else
                {
                    LOGGER.trace("logged in user : " + userFromToken.toString());
                }
                
                JournalDTO journal = journalRepository.findById(journalId);
                if(journal == null)
                {
                    LOGGER.trace("No journal found associated to the requested journalId. Not Found");
                    responseMap.put("statusCode", 404);
                    responseMap.put("statusMessage", "Not Found");
                    responseMap.put("message", "No journal found associated to the requested journalId. Not Found");
                    return responseMap;
                    
                }
                else
                {
                    LOGGER.trace("journal data : " + journal.toString());
                }
                
                if(!journal.getUserId().equals(userFromToken.getId()))
                {
                    LOGGER.trace("Logged in user is not the author of the journal. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user is not the author of the journal. Forbidden");
                    return responseMap;
                }
                
                if(journal.getIsPosted())
                {
                    LOGGER.trace("User journal posted as a writeup");
                    // TO-DO delete post associated to journal
                }
                
                // delete the journal data from database
                journalRepository.deleteById(journalId);
                LOGGER.trace("Journal deleted successfully. Successful");
                responseMap.put("statusCode", 200);
                responseMap.put("statusMessage", "Successful");
                responseMap.put("message", "Journal deleted successfully. Successful");
                
		    }
		    else
		    {
		        LOGGER.trace("The user is not authroized to perform requested action. Unauthorized");
		        responseMap.put("statusCode", 401);
	            responseMap.put("statusMessage", "Unauthorized");
	            responseMap.put("message", "The user is not authroized to perform requested action. Unauthorized");
	            return responseMap;
		    }
		}
		catch(Exception err)
		{
		    LOGGER.error("Error in deleteJournalService service : " + err);
            responseMap.put("statusCode", 500);
            responseMap.put("statusMessage", "Server error");
            responseMap.put("message", "Error in deleteJournalService service : " + err);
		}

		LOGGER.trace("Response from deleteJournal service : " + responseMap);
		return responseMap;
	}

	/**
     * 
     * Service method to get user journals filtered by requested month
     * 
     * @author Akshay
     * @param journalId
     * @param token
     * @return
     */
    public Map<String, Object> getMonthJournalsService(String month, String year, String token)
    {
        LOGGER.trace("In getMonthJournalsService... month : " + month);
        Map<String, Object> responseMap = new HashMap<>();
        List<JournalDTO> journals = new ArrayList<>();
        
        try
        {
            if(authConsumer.isTokenValid(token.substring(7)))
            {
                LOGGER.trace("Authorized");
                
                // Get logged in user
                UserDTO userFromToken = authConsumer.getCurrentUserFromToken(token);
                if(userFromToken == null)
                {
                    LOGGER.trace("Logged in user data not found. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user data not found. Forbidden");
                    return responseMap;
                }
                else
                {
                    LOGGER.trace("logged in user : " + userFromToken.toString());
                }
                
                journals = journalRepository.findByMonthAndYear(userFromToken.getId(), month, year);
                if(journals.size() == 0)
                {
                    LOGGER.trace("There are no journals present for this month. No Content");
                    responseMap.put("statusCode", "204");
                    responseMap.put("statusMessage", "No Content");
                    responseMap.put("message", "There are no journals present for this month. No Content");
                    responseMap.put("month", month);
                    responseMap.put("year", year);
                }
                else
                {
                    LOGGER.trace("Successfully fetched journal data for the requested month. Successful");
                    responseMap.put("statusCode", "200");
                    responseMap.put("statusMessage", "Successful");
                    responseMap.put("message", "Successfully fetched journal data for the requested month. Successful");
                    responseMap.put("month", month);
                    responseMap.put("year", year);
                    responseMap.put("journals", journals);
                }
                
            }
            else
            {
                LOGGER.trace("The user is not authorized to perform requested action. Unauthorized");
                responseMap.put("statusCode", 401);
                responseMap.put("statusMessage", "Unauthorized");
                responseMap.put("message", "The user is not authorized to perform requested action. Unauthorized");
                return responseMap;
            }
        }
        catch(Exception err)
        {
            LOGGER.error("Error in likeUserWriteupService : " + err);
            responseMap.put("statusCode", 500);
            responseMap.put("statusMessage", "Server error");
            responseMap.put("messge", "Error in likeUserWriteupService : " + err);
            return responseMap;
        }
        
        LOGGER.trace("Response from getMonthJournalsService : " + responseMap);
        return responseMap;
    }
    

	/**
	 * 
	 * Service method to update a user journal
	 * 
	 * @author Akshay
	 * @param journalUpdateRequestDto
	 * @param token
	 * @return Map<String, Object> responseMap
	 */
	public Map<String, Object> updateJournalService(JournalUpdateRequestDTO journalUpdateRequestDto, String token)
	{
		LOGGER.trace("In updateJournalService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
		    // Check the length of content in journalContent
            if(journalUpdateRequestDto.getJournalContent().length() > 14999)
            {
                LOGGER.trace("Content length exceeds 14999 characters. Bad Request");
                responseMap.put("statusCode", 400);
                responseMap.put("statusMessage", "Bad Request");
                responseMap.put("message", "Content length exceeds 14999 characters. Bad Request");
                return responseMap;
            }
            
			// Check if the token is valids
			if (authConsumer.isTokenValid(token.substring(7)))
			{
			    LOGGER.trace("Authorized");
			    
			    // Get logged in user
                UserDTO userFromToken = authConsumer.getCurrentUserFromToken(token);
                if(userFromToken == null)
                {
                    LOGGER.trace("Logged in user data not found. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user data not found. Forbidden");
                    return responseMap;
                }
                else
                {
                    LOGGER.trace("logged in user : " + userFromToken.toString());
                }
			    
				// Update user journal
				String journalId = journalUpdateRequestDto.getJournalId();
				LOGGER.trace("Requested journalId : " + journalId);
				JournalDTO currentJournal = journalRepository.findById(journalId);
				if(currentJournal == null)
				{
				    LOGGER.trace("User journal associated to the requested journalId not found. Not Found");
				    responseMap.put("statusCode", 404);
                    responseMap.put("statusMessage", "Not Found");
                    responseMap.put("message", "User journal associated to the requested journalId not found. Not Found");
                    return responseMap;
				}
				else 
				{
				    LOGGER.trace("exisiting journal : " + currentJournal.toString());
				}
				
				Date currentDate = postServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + currentDate);
				
				currentJournal.setUpdatedDate(currentDate);
				currentJournal.setJournalContent(journalUpdateRequestDto.getJournalContent());
				currentJournal.setJournalTitle(journalUpdateRequestDto.getJournalTitle());
				currentJournal.setHashtags(journalUpdateRequestDto.getHashtags());
				LOGGER.trace("updated Journal : " + currentJournal);
				journalRepository.save(currentJournal);
				
				LOGGER.trace("User journal updated successfully. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
	            responseMap.put("message", "User journal updated successfully. Successful");
				
			}
			else
			{
			    LOGGER.trace("The user is not authorized to perform requested action. Unauthorized");
                responseMap.put("statusCode", 401);
                responseMap.put("statusMessage", "Unauthorized");
                responseMap.put("message", "The user is not authorized to perform requested action. Unauthorized");
                return responseMap;
			}
		}
		catch (Exception err)
		{
		    LOGGER.error("Error in updateJournalService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in updateJournalService : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from updateJournalService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get user journal data
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUserJournalService(String journalId, String token)
	{
		LOGGER.trace("In getUserJournalService");
		Map<String, Object> responseMap = new HashMap<>();
		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("User authorized");
				
				// Get logged in user
                UserDTO userFromToken = authConsumer.getCurrentUserFromToken(token);
                if(userFromToken == null)
                {
                    LOGGER.trace("Logged in user data not found. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user data not found. Forbidden");
                    return responseMap;
                }
                else
                {
                    LOGGER.trace("logged in user : " + userFromToken.toString());
                }

				JournalDTO journal = journalRepository.findById(journalId);
				if(journal == null)
				{
				    LOGGER.trace("No Journal found associated to the requested journalId. Not Found");
				    responseMap.put("statusCode", 404);
                    responseMap.put("statusMessage", "Not Found");
                    responseMap.put("message", "No Journal found associated to the requested journalId. Not Found");
                    return responseMap;
				}
				else
				{
				    LOGGER.trace("journal data associated to the requested journalId : " + journal.toString());
				}
				
				if(!journal.getUserId().equals(userFromToken.getId()))
				{
				    LOGGER.trace("Logged in user is not the author to requested journal. Forbidden");
				    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message", "Logged in user is not the author to requested journal. Forbidden");
                    return responseMap;
				}
				
				LOGGER.trace("Successfully fetched user journal associated to requested journalId. Successful");
				responseMap.put("journal", journal);
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Successfully fetched user journal associated to requested journalId. Successful");
			}
			else
			{
				LOGGER.trace("The user is not authorized to perform requested action. Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "The user is not authorized to perform requested action. Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUserJournalService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from getUserJournalService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to create user write up
	 * 
	 * @author Akshay
	 * @param writeUpRequest
	 * @param token
	 * @return
	 */
	public Map<String, Object> createUserWriteupService(WriteUpRequestDTO writeUpRequest, String token)
	{
		LOGGER.trace("In Service method to create user write up");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Username from token : " + usernameFromToken);
				if (!usernameFromToken.equals(writeUpRequest.getUsername()))
				{
					LOGGER.trace("The user is not authorized to create a writeup");
					responseMap.put("statusCode", 403);
					responseMap.put("message", "Unauthorized");
					return responseMap;
				}

				WriteUpDTO writeUpDto = new WriteUpDTO();
				UUID uuid = UUID.randomUUID();
				writeUpDto.setId(String.valueOf(uuid));
				writeUpDto.setUsername(writeUpRequest.getUsername());
				writeUpDto.setWriteupTitle(writeUpRequest.getWriteupTitle());
				writeUpDto.setWriteupText(writeUpRequest.getWriteupText());
				writeUpDto.setLikes(new ArrayList<String>());
				List<WriteUpComment> comments = new ArrayList<>();
				writeUpDto.setComments(comments);
				writeUpDto.setDate(postServiceHelper.parseFormattedDate());
				writeUpDto.setUpdateDate(postServiceHelper.parseFormattedDate());

				writeupRepository.save(writeUpDto);

				responseMap.put("statusCode", 201);
				responseMap.put("message", "Successfully created user writeup");

			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in likeJournalService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from createUserWriteupService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get a user writeup
	 * 
	 * @author Akshay
	 * @param writeupId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUserWriteupByIdService(String writeupId, String token)
	{
		LOGGER.trace("In Service method getUserWriteupByIdService");
		Map<String, Object> responseMap = new HashMap<>();
		WriteUpDTO writeUp = null;
		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Token is valid");
				writeUp = writeupRepository.findById(writeupId);
				LOGGER.trace("writeup date : " + writeUp);
				if (writeUp == null)
				{
					LOGGER.trace("No Content found for writeup");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "No Content found for writeup");
					return responseMap;
				}

				responseMap.put("writeup", writeUp);
				responseMap.put("message", "Successful");
				responseMap.put("statusCode", 200);

			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUserWriteupByIdService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from getUserWriteupByIdService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get all the user writeups of a user by username
	 * 
	 * @author Akshay
	 * @param username
	 * @param token
	 * @return Map<String, Object> containing and status and writeup data list
	 */
	public Map<String, Object> getUserWriteupByUsernameService(String username, String token)
	{
		LOGGER.trace("In Service method getUserWriteupByUsernameService");
		Map<String, Object> responseMap = new HashMap<>();
		List<WriteUpDTO> userWriteups = new ArrayList<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("User is authorized");

				if (username == null || username == "")
				{
					LOGGER.trace("No username present in request. Bad request");
					responseMap.put("statusCode", 400);
					responseMap.put("message", "Bad request");
					return responseMap;
				}

				userWriteups = writeupRepository.findAllByUsername(username);
				if (userWriteups == null || userWriteups.size() == 0)
				{
					LOGGER.trace("No user writeups present");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "No Content found for user writeups");
					return responseMap;
				}

				responseMap.put("writeups", userWriteups);
				responseMap.put("message", "Successful");
				responseMap.put("statusCode", 200);
			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUserWriteupByUsernameService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from getUserWriteupByUsernameService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to edit user writeup
	 * 
	 * @author Akshay
	 * @param editWriteUpRequestDto
	 * @param token
	 * @return
	 */
	public Map<String, Object> editUserWriteupService(EditWriteUpRequestDTO editWriteUpRequestDto, String token)
	{
		LOGGER.trace("In Service method editUserWriteupService");
		Map<String, Object> responseMap = new HashMap<>();
		WriteUpDTO writeUp = null;

		if (editWriteUpRequestDto.getWriteupId() == null || editWriteUpRequestDto.getUsername() == null)
		{
			LOGGER.trace("No username or writeupId present in the request. Bad request");
			responseMap.put("statusCode", 400);
			responseMap.put("message", "Bad Request");
			return responseMap;
		}

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				// Check if user is authorized to edit the writeup
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("username from token : " + usernameFromToken);
				if (!usernameFromToken.equals(editWriteUpRequestDto.getUsername()))
				{
					LOGGER.trace(
							"username from the request is not the same as the logged in user. Hence, cannot edit writeup");
					responseMap.put("statusCode", 403);
					responseMap.put("message", "Forbidden");
					return responseMap;
				}

				// Get the existing writeup
				writeUp = writeupRepository.findById(editWriteUpRequestDto.getWriteupId());
				if (writeUp == null)
				{
					LOGGER.trace("No Writeup found");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "No Content");
					return responseMap;
				}

				writeUp.setWriteupText(editWriteUpRequestDto.getUpdatedWriteUpText());
				writeUp.setUpdateDate(postServiceHelper.parseFormattedDate());
				LOGGER.trace("updated writeup : " + writeUp);
				writeupRepository.save(writeUp);

				responseMap.put("statusCode", 200);
				responseMap.put("message", "User writeup updated successfully");

			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in editUserWriteupService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from editUserWriteupService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to delete a user writeup
	 * 
	 * @author Akshay
	 * @param writeupId
	 * @param token
	 * @return
	 */
	public Map<String, Object> deleteUserWriteupService(String writeupId, String username, String token)
	{
		LOGGER.trace("In Service method deleteUserWriteupService");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				// Check if the user is authorized to delete writeup
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Username from token : " + usernameFromToken);
				if (!username.equals(usernameFromToken))
				{
					LOGGER.trace("The user is not authorized");
					responseMap.put("statusCode", 401);
					responseMap.put("message", "Unauthorized");
					return responseMap;
				}

				LOGGER.trace("Deleting user writeup");
				writeupRepository.deleteById(writeupId);

				responseMap.put("statusCode", 200);
				responseMap.put("message", "Successfully deleted user writeup");
			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 403);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in deleteUserWriteupService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from deleteUserWriteupService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to like a user writeup
	 * 
	 * @author Akshay
	 * @param writeupId
	 * @param username
	 * @param token
	 * @return
	 */
	public Map<String, Object> likeUserWriteupService(String writeupId, String token)
	{
		LOGGER.trace("In Service method likeUserWriteupService");
		Map<String, Object> responseMap = new HashMap<>();
		WriteUpDTO writeup = null;
		List<String> likesList = new ArrayList<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				// Get username from token
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("username from token : " + usernameFromToken);

				writeup = writeupRepository.findById(writeupId);
				LOGGER.trace("user writeup : " + writeup);
				if (writeup == null)
				{
					LOGGER.trace("No user writeup found");
					responseMap.put("statusCode", 204);
					responseMap.put("message", "No Writeup content found");
					return responseMap;
				}

				likesList = writeup.getLikes();
				LOGGER.trace("Current likes : " + likesList);
				if (!likesList.contains(usernameFromToken))
				{
					LOGGER.trace("Adding user like for the writeup");
					likesList.add(usernameFromToken);
					writeup.setLikes(likesList);
					writeupRepository.save(writeup);
				}
				else
				{
					LOGGER.trace("User's like already present for the write");
				}
				responseMap.put("statusCode", 200);
				responseMap.put("message", "Successfully liked user writeup");

			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in likeUserWriteupService : " + err.getMessage());
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			return responseMap;
		}

		LOGGER.trace("Response from likeUserWriteupService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get all the user writeups from database
	 * 
	 * @author Akshay
	 * @param token
	 * @return WriteupResponseWrapper
	 */
	public WriteupResponseWrapper getAllWriteupsService(String token)
	{
		LOGGER.trace("in getAllWriteupsService");
		WriteupResponseWrapper responseWrapper = new WriteupResponseWrapper();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				responseWrapper.setWriteups(writeupRepository.findAll());
				if (responseWrapper.getWriteups().size() > 0)
				{
					responseWrapper.setStatusCode(200);
				}
				else
				{
					responseWrapper.setStatusCode(204);
				}
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseWrapper.setStatusCode(403);
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getAllWriteupsService : " + err.getMessage());
			responseWrapper.setStatusCode(500);
		}

		return responseWrapper;
	}

	

}
