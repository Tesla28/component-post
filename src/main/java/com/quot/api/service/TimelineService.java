package com.quot.api.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.dto.Following;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.profile.consumer.UserProfileConsumer;
import com.quot.api.redis.timeline.entity.TimelineEntity;
import com.quot.api.redis.timeline.repository.RedisHomeTimelineRepository;
import com.quot.api.redis.timeline.repository.RedisUserTimelineRepository;
import com.quot.api.redis.timeline.service.UserHomeTimelineService;
import com.quot.api.redis.timeline.service.UserTimelineService;
import com.quot.api.service.helper.PostDateComparator;
import com.quot.api.service.helper.TimelinePostComparator;
import com.quot.api.user.consumer.UserConsumer;
import com.quot.api.user.dto.UserDTO;

@Service
public class TimelineService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TimelineService.class);

    @Autowired
    private AuthConsumer authConsumer;

    @Autowired
    private UserConsumer userConsumer;

    @Autowired
    private PostService postService;

    @Autowired
    private UserTimelineService userTimelineService;

    @Autowired
    private UserHomeTimelineService userHomeTimelineService;

    @Autowired
    private UserProfileConsumer userProfileConsumer;

    @Autowired
    private RedisHomeTimelineRepository redisHomeTimelineRepository;

    @Autowired
    private RedisUserTimelineRepository redisUserTimelineRepository;

    /**
     * Service method to get user home timepline posts
     * 
     * @author Akshay
     * @param token
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> getHomeTimelineService(String token, int offset, int limit)
    {
        LOGGER.trace("in getHomeTimelineService");
        Map<String, Object> responseMap = new HashMap<>();

        try
        {
            if (authConsumer.isTokenValid(token.substring(7)))
            {
                LOGGER.trace("Authorized");

                // Get user details from token
                UserDTO loggedInUser = userConsumer.getUserFromToken(token);
                if (loggedInUser == null)
                {
                    LOGGER.trace("No user found");
                    responseMap.put("statusCode", 406);
                    responseMap.put("statusMessage", "Not Acceptable");
                    responseMap.put("message", "User associated to requested token not found. Not Acceptable");
                    return responseMap;
                } else
                {
                    LOGGER.trace("User data : " + loggedInUser.toString());
                }

                if (!loggedInUser.isProfilePresent())
                {
                    LOGGER.trace("Logged in user does not have profile associated to the user account. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message",
                        "Logged in user does not have profile associated to the user account. Forbidden");
                    return responseMap;
                }

                // Get profile associated to the user
                ProfileDTO loggedInUserProfile = userProfileConsumer.getUserProfile(token);
                LOGGER.trace("Profile associated to the user : " + loggedInUserProfile.toString());

                if (loggedInUserProfile.getFollowings().size() == 0)
                {
                    LOGGER.trace("User is not following any other user's profile. Conflict");
                    responseMap.put("statusCode", 409);
                    responseMap.put("statusMessage", "Conflict");
                    responseMap.put("message", "User is not following any other user's profile. Conflict");
                    return responseMap;
                }

                List<PostDTO> userPosts = new ArrayList<>();
                List<TimelineEntity> userHomeTimelinePosts =
                    redisHomeTimelineRepository.findAll("hometimeline_" + loggedInUser.getId(), offset, limit);
                LOGGER.trace("user home timeline posts : " + userHomeTimelinePosts);
                if (userHomeTimelinePosts.size() == 0)
                {
                    LOGGER.trace("No posts found in cache");
                    for (Following eachFollowing : loggedInUserProfile.getFollowings())
                    {
                        LOGGER.trace("Current following : " + eachFollowing.toString());
                        Map<String, Object> getUserPostsResponseMap =
                            postService.getUserPostsService(eachFollowing.getUserId(), offset, limit, token);
                        LOGGER.trace("getUserPostsResponseMap : " + getUserPostsResponseMap);
                        if (getUserPostsResponseMap.get("statusCode").equals(new Integer(200)))
                        {
                            userPosts.addAll((List<PostDTO>) getUserPostsResponseMap.get("posts"));
                        }
                        // userHomeTimelinePosts.addAll((List<TimelineEntity>) getUserPostsResponseMap.get("posts"));

                    }

                    

					if(userPosts.size() != 0)
					{
						LOGGER.trace("Adding the posts to user's timeline");
						
						Collections.sort(userPosts, new PostDateComparator());
	                    LOGGER.trace("userHomeTimelinePosts from direct DB call : " + userPosts);
						
						this.addPostsToTimeLine(userPosts, "hometimeline_", token);
						
						responseMap.put("posts", userPosts);
						responseMap.put("statusCode", 200);
						responseMap.put("statusMessage", "Successful");
						responseMap.put("message", "Successfully fetched posts for the user. Successful");
						return responseMap;
					}
					else
					{
						LOGGER.trace("No home timeline posts present for user. No Content");
						responseMap.put("message", "No home timeline posts present for user. No Content");
						responseMap.put("statusCode", 204);
						responseMap.put("statusMessage", "No Content");
						return responseMap;
					}

                }

                Collections.sort(userHomeTimelinePosts, new TimelinePostComparator());
                responseMap.put("posts", userHomeTimelinePosts);
                responseMap.put("statusCode", 200);
                responseMap.put("statusMessage", "Successful");

            } else
            {
                LOGGER.trace("The user is not authorized");
                responseMap.put("statusCode", 401);
                responseMap.put("statusMessage", "Unauthorized");
                responseMap.put("message", "User is not authorized to perform requested task");
                return responseMap;
            }
        }
        catch (Exception err)
        {
            LOGGER.error("Error in getHomeTimelineService service : " + err.getMessage());
            responseMap.put("statusCode", 500);
            responseMap.put("message", "Server error");
            return responseMap;
        }

        LOGGER.trace("Response from getHomeTimelineService : " + responseMap);
        return responseMap;
    }

    /**
     * Service method to get user's timepline posts
     * 
     * @author Akshay
     * @param token
     * @param offset
     * @param limit
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> getUserTimelineService(String token, int offset, int limit)
    {
        LOGGER.trace("in getUserTimelineService");
        Map<String, Object> responseMap = new HashMap<>();

        try
        {
            if (authConsumer.isTokenValid(token.substring(7)))
            {
                LOGGER.trace("Authorized");

                // Get user details from token
                UserDTO loggedInUser = userConsumer.getUserFromToken(token);
                if (loggedInUser == null)
                {
                    LOGGER.trace("No user found");
                    responseMap.put("statusCode", 406);
                    responseMap.put("statusMessage", "Not Acceptable");
                    responseMap.put("message", "User associated to requested token not found. Not Acceptable");
                    return responseMap;
                } else
                {
                    LOGGER.trace("User data : " + loggedInUser.toString());
                }

                if (!loggedInUser.isProfilePresent())
                {
                    LOGGER.trace("Logged in user does not have profile associated to the user account. Forbidden");
                    responseMap.put("statusCode", 403);
                    responseMap.put("statusMessage", "Forbidden");
                    responseMap.put("message",
                        "Logged in user does not have profile associated to the user account. Forbidden");
                    return responseMap;
                }

                List<TimelineEntity> userTimelinePosts = redisUserTimelineRepository
                    .getAllUserTimePosts("usertimeline_" + loggedInUser.getId(), offset, limit);
                LOGGER.trace("user home timeline posts : " + userTimelinePosts);
                List<PostDTO> userPosts = new ArrayList<>();
                if (userTimelinePosts.size() == 0)
                {
                    LOGGER.trace("No posts found in cache");
                    Map<String, Object> getUserPostsResponseMap =
                        postService.getUserPostsService(loggedInUser.getId(), offset, limit, token);
                    LOGGER.trace("getUserPostsResponseMap : " + getUserPostsResponseMap);
                    userPosts.addAll((List<PostDTO>) getUserPostsResponseMap.get("posts"));
                    // userTimelinePosts = (List<TimelineEntity>) getUserPostsResponseMap.get("posts");
                    LOGGER.trace("userHomeTimelinePosts from direct DB call : " + userPosts);

                    Collections.sort(userPosts, new PostDateComparator());

                    if (userPosts.size() != 0)
                    {
                        LOGGER.trace("Adding the posts to user's timeline");
                        this.addPostsToTimeLine(userPosts, "usertimeline_", token);

                        responseMap.put("posts", userPosts);
                        responseMap.put("statusCode", 200);
                        responseMap.put("statusMessage", "Successful");
                        return responseMap;
                    } else
                    {
                        responseMap.put("message", "No user timeline posts present for the user");
                        responseMap.put("statusCode", 204);
                        responseMap.put("statusMessage", "Successful");
                        return responseMap;
                    }

                }
                Collections.sort(userTimelinePosts, new TimelinePostComparator());
                responseMap.put("posts", userTimelinePosts);
                responseMap.put("statusCode", 200);
                responseMap.put("statusMessage", "Successful");

            } else
            {
                LOGGER.trace("The user is not authorized");
                responseMap.put("statusCode", 401);
                responseMap.put("statusMessage", "Unauthorized");
                responseMap.put("message", "User is not authorized to perform requested task");
                return responseMap;
            }
        }
        catch (Exception err)
        {
            LOGGER.error("Error in getUserTimelineService service : " + err.getMessage());
            responseMap.put("statusCode", 500);
            responseMap.put("message", "Server error");
            return responseMap;
        }

        LOGGER.trace("Response from getUserTimelineService : " + responseMap);
        return responseMap;
    }

    /**
     * Async method to add the posts fetched from database to user timeline
     * 
     * @author Akshay
     * @param userTimelinePosts
     * @param string
     */
    @Async
    private void addPostsToTimeLine(List<PostDTO> userPosts, String timelineInitial, String token)
    {
        LOGGER.trace("in addPostsToTimeLine");

        for (PostDTO eachPost : userPosts)
        {
            LOGGER.trace("Current user post : " + eachPost.toString());

            // List<PageDTO> pages = new ArrayList<>();
            // for (TimelinePageDTO eachTimeLinePage : eachTimelineEntity.getPages())
            // {
            // LOGGER.trace("Current timeline page : " + eachTimelineEntity);
            // PageDTO eachPage = new PageDTO();
            // eachPage.setPageText(eachTimeLinePage.getPageText());
            // eachPage.setPageNumber(eachTimeLinePage.getPageNumber());
            // eachPage.setPageTitle(eachTimeLinePage.getPageTitle());
            // pages.add(eachPage);
            // }
            //
            // PostDTO userPost = new PostDTO();
            // userPost.setId(eachTimelineEntity.getId());
            // userPost.setPostType(eachTimelineEntity.getPostType());
            // userPost.setUserId(eachTimelineEntity.getUserId());
            // userPost.setDraft(false);
            // userPost.setPostTitle(eachTimelineEntity.getPostTitle());
            // userPost.setPostText(eachTimelineEntity.getPostText());
            // userPost.setPaged(eachTimelineEntity.isPaged());
            // userPost.setPageCount(eachTimelineEntity.getPageCount());
            // userPost.setPages(pages);
            // userPost.setTotalCharacterCount(eachTimelineEntity.getTotalCharacterCount());
            // userPost.setJournalDate(eachTimelineEntity.getJournalDate());
            // userPost.setHashtags(eachTimelineEntity.getHashtags());
            // userPost.setUpvotes(eachTimelineEntity.getUpvotes());
            // userPost.setAppreciates(eachTimelineEntity.getAppreciates());
            // userPost.setComments(eachTimelineEntity.getComments());
            // userPost.setDate(eachTimelineEntity.getDate());
            // userPost.setUpdatedDate(eachTimelineEntity.getUpdatedDate());
            // LOGGER.trace("User Post data object formed. Now adding to the user and home timelines");
            
            if(timelineInitial.equals("hometimeline_"))
            {
                LOGGER.trace("The request is raised to add the post to user home time");
                userHomeTimelineService.addPostToHomeTimeline(eachPost, token);
            }
            else
            {
                LOGGER.trace("The request is raised to add the post to user time");
                userTimelineService.addToUserTimeline(eachPost, token);
            }
           
        }

    }

}
