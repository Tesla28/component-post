package com.quot.api.constant;

public interface PostConstants
{
	// Post types
	public static final String POST_TYPE_QUICK_READ = "quick-read";

	public static final String POST_TYPE_WRITEUP = "writeup";

	public static final String POST_TYPE_INVALID = "invalid_type";
	
	// Data unique identifer constants 
	public static final String COMMENT_ID_INITIAL = "comment_";
	
	public static final String USER_ID_INITIAL = "user_";
	
	public static final String PROFILE_ID_INITIAL = "profile_";
	
	// Cache constants
	public static final String CACHE_POST = "cache-post";
	
	public static final String CACHE_QUOTE = "cache-quote";

	public static final String CACHE_WRITEUP = "cache-writeup";
	
	public static final String REDIS_USER_DASHBOARD_VALUE = "-dashboard";
	
	public static final String REDIS_HASH_DASHBOARD_POST = "user-dashboard";

	// Hashtag constants
	public static final String DEFAULT_TAG_QUICK_READ = "#quickread";

	public static final String DEFAULT_TAG_USER_QUICK_READ = "#user_quickread";
	
	public static final String DEFAULT_TAG_WRITEUP = "#writeup";

	public static final String DEFAULT_TAG_USER_WRITEUP = "#user_writeup";
	
}
