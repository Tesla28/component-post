package com.quot.api.redis.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.quot.api.dto.Follower;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.profile.consumer.UserProfileConsumer;
import com.quot.api.user.dto.UserDTO;

@Service
public class AddPostRedisServiceHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AddPostRedisServiceHelper.class);

	@Autowired
	private UserProfileConsumer userProfileConsumer;
	
//	@Autowired
//	private RedisMessagePublisher redisMessagePublisher;
	
	/**
	 * 
	 * Redis Helper method to send added post to all user's followers
	 * 
	 * @author Akshay
	 * @param user
	 */
//	@Async
//	public void sendPostToSubscribers(PostDTO userPost, UserDTO user, String token)
//	{
//		LOGGER.trace("in sendPostToSubscribers");
//		
//		// Get profile associated to the user
//		ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
//		LOGGER.trace("Profile associated to user : " + userProfile.toString());
//		
//		if(userProfile.getFollowers().size()>0)
//		{
//			LOGGER.trace("User has followers");
//			List<Follower> followers = userProfile.getFollowers();
//			
//			for(Follower eachFollower : followers)
//			{
//				LOGGER.trace("Current follower : " + eachFollower.toString());
//				redisMessagePublisher.publish(userPost.toString());
//			}
//		}
//		else
//		{
//			LOGGER.trace("User has no followers");
//		}
//		
//	}
	
}
