package com.quot.api.redis.timeline.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.quot.api.dto.PostDTO;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.profile.consumer.UserProfileConsumer;
import com.quot.api.redis.timeline.entity.TimelineEntity;
import com.quot.api.redis.timeline.repository.RedisHomeTimelineRepository;
import com.quot.api.redis.timeline.repository.RedisUserTimelineRepository;

@Service
public class UserTimelineService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserTimelineService.class);
	
	@Autowired
	private UserProfileConsumer userProfileConsumer;
	
	@Autowired
	private RedisUserTimelineRepository redisUserTimelineRepository;
	
	@Autowired 
	private RedisHomeTimelineRepository redisHomeTimelineRepository;

	
	/**
	 * 
	 * Service method to add user's post to user timeline
	 * 
	 * @author Akshay
	 * @param userPost
	 * @param token
	 */
	@Async
	public void addToUserTimeline(PostDTO userPost, String token)
	{
		LOGGER.trace("in addToUserTimeline");

		// Get profile associated to the user
		ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
		LOGGER.trace("Profile associated to user : " + userProfile.toString());
		
		TimelineEntity timelineEntity = new TimelineEntity();
		timelineEntity.setId(userPost.getId());
		timelineEntity.setUserId(userPost.getUserId());
		timelineEntity.setAuthorUserId(userPost.getUserId());
		timelineEntity.setPostType(userPost.getPostType());
		timelineEntity.setPostTitle(userPost.getPostTitle());
		timelineEntity.setPostText(userPost.getPostText());
		timelineEntity.setTotalCharacterCount(userPost.getTotalCharacterCount());
		timelineEntity.setPaged(userPost.isPaged());
		timelineEntity.setPageCount(userPost.getPageCount());
		timelineEntity.setComments(userPost.getComments());
		timelineEntity.setHashtags(userPost.getHashtags());
		timelineEntity.setJournalDate(userPost.getJournalDate());
		timelineEntity.setUpvotes(userPost.getUpvotes());
		timelineEntity.setDate(userPost.getDate());
		timelineEntity.setUpdatedDate(userPost.getUpdatedDate());
		LOGGER.trace("Timeline entity : " + timelineEntity.toString());
		
		redisUserTimelineRepository.save(timelineEntity);
		LOGGER.trace("Added date to user timeline");
	}
	
	/**
	 * 
	 * Service method to delete user's post from user timeline
	 * 
	 * @author Akshay
	 * @param post
	 * @param token
	 */
	@Async
	public void deletePostEntryFromUserTimeline(PostDTO post, String token)
	{
		LOGGER.trace("in deletePostEntryFromUserTimeline");
		
		// Get profile associated to the user
        ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
        LOGGER.trace("Profile associated to user : " + userProfile.toString());
		
		TimelineEntity timelineEntity = new TimelineEntity();
		timelineEntity.setId(post.getId());
		timelineEntity.setUserId(post.getUserId());
		timelineEntity.setAuthorUserId(userProfile.getUserId());
		timelineEntity.setPostType(post.getPostType());
		timelineEntity.setPostTitle(post.getPostTitle());
		timelineEntity.setPostText(post.getPostText());
		timelineEntity.setTotalCharacterCount(post.getTotalCharacterCount());
		timelineEntity.setPaged(post.isPaged());
		timelineEntity.setPageCount(post.getPageCount());
		timelineEntity.setComments(post.getComments());
		timelineEntity.setHashtags(post.getHashtags());
		timelineEntity.setJournalDate(post.getJournalDate());
		timelineEntity.setUpvotes(post.getUpvotes());
		timelineEntity.setDate(post.getDate());
		timelineEntity.setUpdatedDate(post.getUpdatedDate());
		LOGGER.trace("Timeline entity : " + timelineEntity.toString());
		
		redisUserTimelineRepository.deleteEntity("usertimeline_" + timelineEntity.getUserId(), timelineEntity);
		LOGGER.trace("Successfully deleted entry from user timeline");
	}


	/**
	 * 
	 * Service method to update user's timeline with latest post data
	 * 
	 * @author Akshay
	 * @param presentPost
	 * @param tempPostDTO
	 * @param token
	 */
	public void updatePostOnUserTimeline(PostDTO presentPost, PostDTO updatedPostDTO, String token)
	{
		LOGGER.trace("in updatePostOnUserTimelines");
		
		// Delete existing post data from user timeline
		deletePostEntryFromUserTimeline(presentPost, token);
		
		// Add updated data to user timeline
		addToUserTimeline(updatedPostDTO, token);
		
		LOGGER.trace("Updated post data to user times");
	}

}
