package com.quot.api.redis.timeline.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@RedisHash("TimelineEntity")
public class TimelineEntity implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 8807143135849279050L;

    @JsonProperty("id")
    private String id;

    @JsonProperty("postType")
    private String postType;

    @JsonProperty("authorUserId")
    private String authorUserId;

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("postTitle")
    private String postTitle;

    @JsonProperty("postText")
    private String postText;

    @JsonProperty("totalCharacterCount")
    private int totalCharacterCount;

    @JsonProperty("isPaged")
    private boolean isPaged;

    @JsonProperty("pageCount")
    private int pageCount;

    @JsonProperty("pages")
    private List<TimelinePageDTO> pages;

    @JsonProperty("hashtags")
    private List<String> hashtags = null;

    @JsonProperty("upvotes")
    private List<String> upvotes = null;

    @JsonProperty("appreciates")
    private List<String> appreciates = null;

    @JsonProperty("comments")
    private List<String> comments = null;

    @JsonProperty("journalDate")
    private Date journalDate;

    @JsonProperty("date")
    private Date date;

    @JsonProperty("updatedDate")
    private Date updatedDate;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId()
    {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id)
    {
        this.id = id;
    }

    @JsonProperty("postType")
    public String getPostType()
    {
        return postType;
    }

    @JsonProperty("postType")
    public void setPostType(String postType)
    {
        this.postType = postType;
    }

    @JsonProperty("authorUserId")
    public String getAuthorUserId()
    {
        return authorUserId;
    }

    @JsonProperty("authorUserId")
    public void setAuthorUserId(String authorUserId)
    {
        this.authorUserId = authorUserId;
    }

    @JsonProperty("userId")
    public String getUserId()
    {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @JsonProperty("postTitle")
    public String getPostTitle()
    {
        return postTitle;
    }

    @JsonProperty("postTitle")
    public void setPostTitle(String postTitle)
    {
        this.postTitle = postTitle;
    }

    @JsonProperty("postText")
    public String getPostText()
    {
        return postText;
    }

    @JsonProperty("postText")
    public void setPostText(String postText)
    {
        this.postText = postText;
    }

    @JsonProperty("totalCharacterCount")
    public int getTotalCharacterCount()
    {
        return totalCharacterCount;
    }

    @JsonProperty("totalCharacterCount")
    public void setTotalCharacterCount(int totalCharacterCount)
    {
        this.totalCharacterCount = totalCharacterCount;
    }

    @JsonProperty("isPaged")
    public boolean isPaged()
    {
        return isPaged;
    }

    @JsonProperty("isPaged")
    public void setPaged(boolean isPaged)
    {
        this.isPaged = isPaged;
    }

    @JsonProperty("pageCount")
    public int getPageCount()
    {
        return pageCount;
    }

    @JsonProperty("pageCount")
    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }

    @JsonProperty("pages")
    public List<TimelinePageDTO> getPages()
    {
        return pages;
    }

    @JsonProperty("pages")
    public void setPages(List<TimelinePageDTO> pages)
    {
        this.pages = pages;
    }

    @JsonProperty("hashtags")
    public List<String> getHashtags()
    {
        return hashtags;
    }

    @JsonProperty("hashtags")
    public void setHashtags(List<String> hashtags)
    {
        this.hashtags = hashtags;
    }

    @JsonProperty("upvotes")
    public List<String> getUpvotes()
    {
        return upvotes;
    }

    @JsonProperty("upvotes")
    public void setUpvotes(List<String> upvotes)
    {
        this.upvotes = upvotes;
    }

    @JsonProperty("appreciates")
    public List<String> getAppreciates()
    {
        return appreciates;
    }

    @JsonProperty("appreciates")
    public void setAppreciates(List<String> appreciates)
    {
        this.appreciates = appreciates;
    }

    @JsonProperty("comments")
    public List<String> getComments()
    {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(List<String> comments)
    {
        this.comments = comments;
    }

    @JsonProperty("journalDate")
    public Date getJournalDate()
    {
        return journalDate;
    }

    @JsonProperty("journalDate")
    public void setJournalDate(Date journalDate)
    {
        this.journalDate = journalDate;
    }

    @JsonProperty("date")
    public Date getDate()
    {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Date date)
    {
        this.date = date;
    }

    @JsonProperty("updatedDate")
    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    @JsonProperty("updatedDate")
    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TimelineEntity other = (TimelineEntity) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
