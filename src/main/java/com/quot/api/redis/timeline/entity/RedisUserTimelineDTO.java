package com.quot.api.redis.timeline.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Id;

import com.quot.api.dto.PostDTO;

public class RedisUserTimelineDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5500308315672959827L;
	
	@Id
	private String entityId;
	
	private List<PostDTO> userTimelinePosts = new ArrayList<>();
	
	
	// No Args Constructor
	public RedisUserTimelineDTO()
	{
		
	}

	// Args Constructor
	public RedisUserTimelineDTO(String entityId, List<PostDTO> userTimelinePosts)
	{
		this.entityId = entityId;
		this.userTimelinePosts = userTimelinePosts;
	}

	public String getEntityId()
	{
		return entityId;
	}

	public void setEntityId(String entityId)
	{
		this.entityId = entityId;
	}

	public List<PostDTO> getUserTimelinePosts()
	{
		return userTimelinePosts;
	}

	public void setUserTimelinePosts(List<PostDTO> userTimelinePosts)
	{
		this.userTimelinePosts = userTimelinePosts;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
	
}
