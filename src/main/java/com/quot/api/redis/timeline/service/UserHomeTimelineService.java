package com.quot.api.redis.timeline.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.quot.api.dto.Follower;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.profile.consumer.UserProfileConsumer;
import com.quot.api.redis.timeline.entity.TimelineEntity;
import com.quot.api.redis.timeline.repository.RedisHomeTimelineRepository;

@Service
public class UserHomeTimelineService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserHomeTimelineService.class);

	@Autowired
	private UserProfileConsumer userProfileConsumer;
	
	@Autowired
	private RedisHomeTimelineRepository redisHomeTimelineRepository;
	
	/**
	 * 
	 * Fanout logic service method to add to all the followers userTimeline
	 * 
	 * @author Akshay
	 * @param userPost
	 * @param token
	 */
	@Async
	public void fanoutToFollowers(PostDTO userPost, String token)
	{
		LOGGER.trace("in fanoutToFollowers");

		// Get profile associated to the user
		ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
		LOGGER.trace("Profile associated to user : " + userProfile.toString());

		if (userProfile.getFollowers().size() > 0)
		{
			LOGGER.trace("User has followers");
			List<Follower> followers = userProfile.getFollowers();
			
			// Add self follower to the followers list to see your update on your home timeline
			Follower selfFollower = new Follower();
			selfFollower.setUserId(userProfile.getUserId());
			selfFollower.setProfileId(userProfile.getId());
			followers.add(selfFollower);
			LOGGER.trace("followers list : " + followers);

			for (Follower eachFollower : followers)
			{
				LOGGER.trace("Current follower : " + eachFollower.toString());
				
				TimelineEntity timelineEntity = new TimelineEntity();
				timelineEntity.setId(userPost.getId());
				timelineEntity.setAuthorUserId(userPost.getUserId());
				timelineEntity.setUserId(eachFollower.getUserId());
				timelineEntity.setPostType(userPost.getPostType());
				timelineEntity.setPostTitle(userPost.getPostTitle());
				timelineEntity.setPostText(userPost.getPostText());
				timelineEntity.setTotalCharacterCount(userPost.getTotalCharacterCount());
				timelineEntity.setPaged(userPost.isPaged());
				timelineEntity.setPageCount(userPost.getPageCount());
				timelineEntity.setComments(userPost.getComments());
				timelineEntity.setHashtags(userPost.getHashtags());
				timelineEntity.setJournalDate(userPost.getJournalDate());
				timelineEntity.setUpvotes(userPost.getUpvotes());
				timelineEntity.setDate(userPost.getDate());
				timelineEntity.setUpdatedDate(userPost.getUpdatedDate());
				LOGGER.trace("Timeline entity : " + timelineEntity.toString());
				
				LOGGER.trace("Redis HomeTimeline data object : "  + timelineEntity);
				redisHomeTimelineRepository.save(timelineEntity);
			}
		}
	}
	
	
	/**
	 * 
	 * Method to add post to user's hometimeline
	 * 
	 * @author Akshay
	 * @param userPost
	 * @param token
	 */
	@Async
    public void addPostToHomeTimeline(PostDTO userPost, String token)
    {
        // Get profile associated to the user
        ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
        LOGGER.trace("Profile associated to user : " + userProfile.toString());

        TimelineEntity timelineEntity = new TimelineEntity();
        timelineEntity.setId(userPost.getId());
        timelineEntity.setUserId(userProfile.getUserId());
        timelineEntity.setAuthorUserId(userPost.getUserId());
        timelineEntity.setPostType(userPost.getPostType());
        timelineEntity.setPostTitle(userPost.getPostTitle());
        timelineEntity.setPostText(userPost.getPostText());
        timelineEntity.setTotalCharacterCount(userPost.getTotalCharacterCount());
        timelineEntity.setPaged(userPost.isPaged());
        timelineEntity.setPageCount(userPost.getPageCount());
        timelineEntity.setComments(userPost.getComments());
        timelineEntity.setHashtags(userPost.getHashtags());
        timelineEntity.setJournalDate(userPost.getJournalDate());
        timelineEntity.setUpvotes(userPost.getUpvotes());
        timelineEntity.setDate(userPost.getDate());
        timelineEntity.setUpdatedDate(userPost.getUpdatedDate());
        LOGGER.trace("Timeline entity : " + timelineEntity.toString());
        
        redisHomeTimelineRepository.save(timelineEntity);
        LOGGER.trace("Added date to user timeline");
    }
	
	
	/**
	 * 
	 * Method to delete requested entity from followers home timeline
	 * 
	 * @author Akshay
	 * @param userPost
	 * @param token
	 */
	@Async
	public void deletePostEntryFromHomeTimeline(PostDTO userPost, String token)
	{
		LOGGER.trace("in deletePostEntryFromHomeTimes");

		// Get profile associated to the user
		ProfileDTO userProfile = userProfileConsumer.getUserProfile(token);
		LOGGER.trace("Profile associated to user : " + userProfile.toString());

		if (userProfile.getFollowers().size() > 0)
		{
			LOGGER.trace("User has followers");
			List<Follower> followers = userProfile.getFollowers();

			for (Follower eachFollower : followers)
			{
				LOGGER.trace("Current follower : " + eachFollower.toString());
				
				TimelineEntity timelineEntity = new TimelineEntity();
				timelineEntity.setId(userPost.getId());
				timelineEntity.setUserId(userPost.getUserId());
				timelineEntity.setAuthorUserId(userProfile.getUserId());
				timelineEntity.setPostType(userPost.getPostType());
				timelineEntity.setPostTitle(userPost.getPostTitle());
				timelineEntity.setPostText(userPost.getPostText());
				timelineEntity.setTotalCharacterCount(userPost.getTotalCharacterCount());
				timelineEntity.setPaged(userPost.isPaged());
				timelineEntity.setPageCount(userPost.getPageCount());
				timelineEntity.setComments(userPost.getComments());
				timelineEntity.setHashtags(userPost.getHashtags());
				timelineEntity.setJournalDate(userPost.getJournalDate());
				timelineEntity.setUpvotes(userPost.getUpvotes());
				timelineEntity.setDate(userPost.getDate());
				timelineEntity.setUpdatedDate(userPost.getUpdatedDate());
				LOGGER.trace("Timeline entity : " + timelineEntity.toString());
				
				redisHomeTimelineRepository.deleteEntity("hometimeline_" + timelineEntity.getUserId() ,timelineEntity);
			}
		}
	}


	/**
	 * 
	 * Service method to update user followers home timeline with updated post data
	 * 
	 * @author Akshay
	 * @param presentPost
	 * @param tempPostDTO
	 * @param token
	 */
	public void updatePostOnHomeTimelines(PostDTO presentPost, PostDTO updatedPostDTO, String token)
	{
		LOGGER.trace("in updatePostOnHomeTimelines");
		
		// Delete old post data from followers home timeline
		deletePostEntryFromHomeTimeline(presentPost, token);
		
		// Add updated post date to user followers home timeline
		fanoutToFollowers(updatedPostDTO, token);
		
	}
	
}
