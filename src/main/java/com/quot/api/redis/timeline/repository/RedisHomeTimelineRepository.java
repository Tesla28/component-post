package com.quot.api.redis.timeline.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.PostDTO;
import com.quot.api.redis.timeline.entity.TimelineEntity;

@Repository
public class RedisHomeTimelineRepository
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RedisHomeTimelineRepository.class);

	@SuppressWarnings("rawtypes")
	@Autowired
	private RedisTemplate homeTimelineTemplate;

	@SuppressWarnings("unchecked")
	public void save(TimelineEntity timelineEntity)
	{
		homeTimelineTemplate.opsForList().leftPush("hometimeline_" + timelineEntity.getUserId(), timelineEntity);
	}

	/**
	 * 
	 * Get all the timeline posts associated to a user
	 * 
	 * @author Akshay
	 * @param entityId
	 * @param offset
	 * @param limit
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TimelineEntity> findAll(String entityId, int offset, int limit)
	{
		ListOperations<String, TimelineEntity> homeTimelinePosts = homeTimelineTemplate.opsForList();

		LOGGER.trace("in home timeline redis repository findAll... entityId : " + entityId);
		try
		{
			return homeTimelinePosts.range(entityId, offset, limit);
		}
		catch (Exception err)
		{
			LOGGER.error("Error in findAll : " + err);
		}
		return null;
	}

	/**
	 * 
	 * Delete a specific entry from home time line
	 * 
	 * @author Akshay
	 * @param entityId
	 * @param userPost
	 */
	@SuppressWarnings("unchecked")
	public void deleteEntity(String entityId, TimelineEntity timelineEntity)
	{
		ListOperations<String, PostDTO> homeTimelinePosts = homeTimelineTemplate.opsForList();
		LOGGER.trace("in home timeline redis repository deleteEntity... entityId : " + entityId);
		homeTimelinePosts.remove(entityId, 1L, timelineEntity);
	}
}
