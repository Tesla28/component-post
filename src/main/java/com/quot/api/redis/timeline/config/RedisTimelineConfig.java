package com.quot.api.redis.timeline.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration.JedisPoolingClientConfigurationBuilder;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.quot.api.redis.timeline.entity.TimelineEntity;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableRedisRepositories
public class RedisTimelineConfig
{

	@Value("${redis.host}")
	private String redisHost;

	@Value("${redis.port}")
	private int redisPort;
	
	@Bean
	public JedisClientConfiguration getJedisClientConfiguration()
	{
		JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jedisConfiguration = (JedisPoolingClientConfigurationBuilder) JedisClientConfiguration
				.builder();
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(50);
		poolConfig.setMaxIdle(20);
		poolConfig.setMinIdle(10);
		poolConfig.setTestOnBorrow(true);
		poolConfig.setTestOnReturn(true);

		return jedisConfiguration.poolConfig(poolConfig).build();
	}

	@Bean
	public JedisConnectionFactory connectionFactory()
	{
		RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
		configuration.setHostName(redisHost);
		configuration.setPort(redisPort);
		return new JedisConnectionFactory(configuration, getJedisClientConfiguration());
	}

	@Bean
	@Qualifier("homeTimelineTemplate")
	public RedisTemplate<String, List<TimelineEntity>> homeTimelineTemplate()
	{
		RedisTemplate<String, List<TimelineEntity>> homeTimelineTemplate = new RedisTemplate<>();
		homeTimelineTemplate.setConnectionFactory(connectionFactory());
		homeTimelineTemplate.setKeySerializer(new StringRedisSerializer());
		homeTimelineTemplate.setHashKeySerializer(new JdkSerializationRedisSerializer());
		// homeTimelineTemplate.setHashKeySerializer(new JdkSerializationRedisSerializer());
		// homeTimelineTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		homeTimelineTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		// userTimelineTemplate.setValueSerializer(new StringRedisSerializer());
		homeTimelineTemplate.setEnableTransactionSupport(true);
		homeTimelineTemplate.afterPropertiesSet();
		return homeTimelineTemplate;
	}

	@Bean
	@Qualifier("userTimelineTemplate")
	public RedisTemplate<String, List<TimelineEntity>> userTimelineTemplate()
	{
		RedisTemplate<String, List<TimelineEntity>> userTimelineTemplate = new RedisTemplate<>();
		userTimelineTemplate.setConnectionFactory(connectionFactory());
		userTimelineTemplate.setKeySerializer(new StringRedisSerializer());
		userTimelineTemplate.setHashKeySerializer(new JdkSerializationRedisSerializer());
		// userTimelineTemplate.setHashKeySerializer(new JdkSerializationRedisSerializer());
		// userTimelineTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		userTimelineTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		// userTimelineTemplate.setValueSerializer(new StringRedisSerializer());
		userTimelineTemplate.setEnableTransactionSupport(true);
		userTimelineTemplate.afterPropertiesSet();
		return userTimelineTemplate;
	}

}
