package com.quot.api.redis.timeline.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TimelinePageDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5234533504309913021L;

	@JsonProperty("pageNumber")
	private Integer pageNumber;

	@JsonProperty("pageTitle")
	private String pageTitle;

	@JsonProperty("pageText")
	private String pageText;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("pageNumber")
	public Integer getPageNumber()
	{
		return pageNumber;
	}

	@JsonProperty("pageNumber")
	public void setPageNumber(Integer pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	@JsonProperty("pageTitle")
	public String getPageTitle()
	{
		return pageTitle;
	}

	@JsonProperty("pageTitle")
	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	@JsonProperty("pageText")
	public String getPageText()
	{
		return pageText;
	}

	@JsonProperty("pageText")
	public void setPageText(String pageText)
	{
		this.pageText = pageText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
