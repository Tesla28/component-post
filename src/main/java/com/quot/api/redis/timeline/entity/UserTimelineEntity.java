package com.quot.api.redis.timeline.entity;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonProperty;

@RedisHash("UserTimelineEntity")
public class UserTimelineEntity implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 5217746078662828257L;

    @JsonProperty("userId")
    private String userId;
    
    @JsonProperty("avatar")
    private String avatar;

    @JsonProperty("userId")
    public String getUserId()
    {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @JsonProperty("avatar")
    public String getAvatar()
    {
        return avatar;
    }

    @JsonProperty("avatar")
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }
    
}
