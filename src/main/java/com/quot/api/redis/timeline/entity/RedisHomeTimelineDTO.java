package com.quot.api.redis.timeline.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import com.quot.api.dto.PostDTO;

@RedisHash("HomeTimeline")
public class RedisHomeTimelineDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5500308315672959827L;
	
	@Id
	private String entityId;
	
	private List<PostDTO> userHomeTimelinePosts = new ArrayList<>();
	
	
	// No Args Constructor
	public RedisHomeTimelineDTO()
	{
		
	}

	// Args Constructor
	public RedisHomeTimelineDTO(String entityId, List<PostDTO> userHomeTimelinePosts)
	{
		this.entityId = entityId;
		this.userHomeTimelinePosts = userHomeTimelinePosts;
	}

	public String getEntityId()
	{
		return entityId;
	}

	public void setEntityId(String entityId)
	{
		this.entityId = entityId;
	}

	public List<PostDTO> getUserHomeTimelinePosts()
	{
		return userHomeTimelinePosts;
	}

	public void setUserHomeTimelinePosts(List<PostDTO> userHomeTimelinePosts)
	{
		this.userHomeTimelinePosts = userHomeTimelinePosts;
	}
	
	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
	
}
