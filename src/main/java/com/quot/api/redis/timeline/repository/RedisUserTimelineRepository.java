package com.quot.api.redis.timeline.repository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.stereotype.Repository;

import com.quot.api.redis.timeline.entity.TimelineEntity;

@Repository
@EnableRedisRepositories
public class RedisUserTimelineRepository
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RedisUserTimelineRepository.class);
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private RedisTemplate userTimelineTemplate;
	
	@SuppressWarnings("unchecked")
	public void save(TimelineEntity userTimelineEntity)
	{
		LOGGER.trace("in RedisUserTimelineRepository's save method");
		ListOperations<String, TimelineEntity> userTimelinePosts = userTimelineTemplate.opsForList();
		userTimelinePosts.leftPush("usertimeline_" + userTimelineEntity.getUserId(), userTimelineEntity);
    }
	
	
	/**
	 * 
	 * Get all the user timeline posts associated to a user
	 * 
	 * @author Akshay
	 * @param entityId
	 * @param offset
	 * @param limit
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TimelineEntity> getAllUserTimePosts(String entityId, int offset, int limit)
	{
		List<TimelineEntity> userPosts = new ArrayList<>();
		ListOperations<String, TimelineEntity> userTimelinePosts = userTimelineTemplate.opsForList();
		
		LOGGER.trace("in user timeline redis repository getAllUserTimePosts... entityId : " + entityId);
		try
		{
			// serializedContent = userTimelinePosts.range(entityId, offset, limit);
			// serializedContent.deserialize(userTimelinePosts.range(entityId, offset, limit));
			userPosts = userTimelinePosts.range(entityId, offset, limit);
			return userPosts;
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getAllUserTimePosts : " + err);
		}
		return null;
	}
	
	
	/**
	 * 
	 * Delete a specific entry from user time line
	 * 
	 * @author Akshay
	 * @param entityId
	 * @param userPost
	 */
	@SuppressWarnings("unchecked")
	public void deleteEntity(String entityId, TimelineEntity userTimelineEntity)
	{
		ListOperations<String, TimelineEntity> userTimelinePosts = userTimelineTemplate.opsForList();
		LOGGER.trace("in home timeline redis repository deleteEntity... entityId : " + entityId);
		Long returnedValue = userTimelinePosts.remove(entityId, 1L, userTimelineEntity);
		LOGGER.trace("deleted entity with key : " + entityId + " and returned Value : " + returnedValue);
	}
}
