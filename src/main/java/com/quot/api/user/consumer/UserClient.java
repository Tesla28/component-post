package com.quot.api.user.consumer;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.quot.api.dto.GetBulkUsersRequestWrapper;

@FeignClient(name = "COMPONENT-USER", url = "localhost:7001")
public interface UserClient
{
	@RequestMapping(path = "/quot/api/user", method = RequestMethod.GET)
	ResponseEntity<Map<String, Object>> getUser(@RequestHeader("Authorization") String token);
	
	@RequestMapping(path = "/quot/api/user/get/bulk-users", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getBulkUsers(@RequestBody GetBulkUsersRequestWrapper requestWrapper,
			@RequestHeader("Authorization") String token);
	
	@RequestMapping(path = "/quot/api/user/loadUser", method = RequestMethod.GET)
	ResponseEntity<Map<String, Object>> getUserById(@RequestParam(name = "userId") String userId ,@RequestHeader("Authorization") String token);
}
