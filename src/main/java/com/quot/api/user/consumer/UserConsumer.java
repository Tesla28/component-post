package com.quot.api.user.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.dto.BulkUserResponse;
import com.quot.api.dto.GetBulkUsersRequestWrapper;
import com.quot.api.user.dto.UserDTO;

@Service
public class UserConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserConsumer.class);
	
	@Autowired
	private UserClient userClient;
	
	/**
	 * 
	 * Get userId from user component
	 * 
	 * @author Akshay
	 * @param userName
	 * @param token
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getUserIdFromUserName(String token)
	{
		LOGGER.trace("in getUserIdFromUserName method of UserConsumer");
		String userId = null;
		Map<String, Object> userDataMap = new HashMap<>();
		ResponseEntity<Map<String, Object>> responseEntity = null;
		try
		{
			responseEntity = userClient.getUser(token);
			LOGGER.trace("Response from user component : " + responseEntity);

			if(responseEntity.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("userId : " + userId);
				userDataMap = (Map<String, Object>) responseEntity.getBody().get("data");
				userId = (String) userDataMap.get("userId");
			}
			
		}
		catch (Exception err)
		{
			LOGGER.error("Error in UserConsumer : " + err.getMessage());
			userId = null;
		}
		
		LOGGER.trace("response userId : " + userId);
		return userId;
	}
	
	/**
	 * 
	 * Get user from username
	 * 
	 * @author Akshay
	 * @param username
	 * @param token
	 * @return UserDTO
	 */
	public UserDTO getUserFromToken(String token)
	{
		LOGGER.trace("in getUserFromUserName method of UserConsumer");
		UserDTO user = null;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<Map<String, Object>> responseEntity = null;
		try
		{
			responseEntity = userClient.getUser(token);
			LOGGER.trace("Response from user component : " + responseEntity);

			if(responseEntity.getStatusCode().equals(HttpStatus.OK))
			{
				user = mapper.convertValue(responseEntity.getBody().get("user"), UserDTO.class);
				LOGGER.trace("user : " + user);
			}
			else 
			{
				LOGGER.trace("User not found");
				user = null;
			}
			
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getUserFromUsername : " + err);
			user = null;
		}
		
		LOGGER.trace("response from getUserFromUsername : " + user);
		return user;
	}
	
	
	/**
	 * 
	 * Consumer method to get request followers/followings for a user
	 * 
	 * @author Akshay
	 * @param followers
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public List<BulkUserResponse> getBulkUsers(List<String> followers, String token)
	{
		LOGGER.trace("in user-consumer's getBulkUsers");
		List<BulkUserResponse> users = new ArrayList<>();
		ResponseEntity<Map<String, Object>> response = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try 
		{
			GetBulkUsersRequestWrapper requestWrapper = new GetBulkUsersRequestWrapper();
			requestWrapper.setUsers(followers);
			response = userClient.getBulkUsers(requestWrapper, token);
			LOGGER.trace("response from component-user : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Response status is 200 OK");
				users = mapper.convertValue(response.getBody().get("users"), ArrayList.class);
			}
			if(response.getStatusCode().equals(HttpStatus.NO_CONTENT))
			{
				LOGGER.trace("Response status is 204 No Content. Users not found for requested username");
				return users;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getBulkUsers : " + err);
		}
		
		LOGGER.trace("Response from getBulkUsers : " + users);
		return users;
	}

	/**
	 * 
	 * User consumer method to get user data from userId
	 * 
	 * @author Akshay
	 * @param userId
	 * @param token
	 * @return
	 */
    public UserDTO getUserFromId(String userId, String token)
    {
        LOGGER.trace("In getUserFromId user consumer method... userId : " + userId);
        ResponseEntity<Map<String, Object>> response = null;
        UserDTO user = new UserDTO();
        
        try
        {
            response = userClient.getUserById(userId, token);
            LOGGER.trace("Response from user component : " + response);

            if(response.getStatusCode().equals(HttpStatus.OK))
            {
                LOGGER.trace("User Component response is 200 OK");
                ObjectMapper mapper = new ObjectMapper();
                user = mapper.convertValue(response.getBody().get("user"), UserDTO.class);
            }
            else
            {
                LOGGER.trace("User Component response is NOT 200 OK");
                user = null;
            }
            
        }
        catch(Exception err)
        {
            LOGGER.error("Error from user component : " + err);
            user = null;
        }
        
        LOGGER.trace("Response from getUserFromId : " + user.toString());
        return user;
    }

}
