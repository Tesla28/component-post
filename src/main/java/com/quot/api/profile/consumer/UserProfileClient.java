package com.quot.api.profile.consumer;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "COMPONENT-PROFILE", url = "localhost:7021")
public interface UserProfileClient
{

	@RequestMapping(path = "/quot/api/profile", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getLoggedInUserProfile(@RequestHeader("Authorization") String token);
}
