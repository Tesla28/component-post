package com.quot.api.profile.consumer;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.dto.ProfileDTO;

@Service
public class UserProfileConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileConsumer.class);

	@Autowired
	private UserProfileClient userProfileClient;
	
	public ProfileDTO getUserProfile(String token) 
	{
		LOGGER.trace("in getUserProfile consumer method");
		ProfileDTO userProfile = new ProfileDTO();
		ResponseEntity<Map<String, Object>> responseEnity = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try 
		{
			responseEnity = userProfileClient.getLoggedInUserProfile(token);
			LOGGER.trace("responseEntity from component-profile : " + responseEnity);
			if(responseEnity.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("status is 200");
				userProfile = mapper.convertValue(responseEnity.getBody().get("profile"), ProfileDTO.class);
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error from component-post : " + err);
			userProfile = null;
		}
		
		LOGGER.trace("response from getUserProfile consumer method : " + userProfile);
		return userProfile;
	}
}
