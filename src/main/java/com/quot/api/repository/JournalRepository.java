package com.quot.api.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.quot.api.dto.JournalDTO;

public interface JournalRepository extends MongoRepository<JournalDTO, Integer>
{
	void deleteById(String journalId);

	JournalDTO findById(String journalId);

	JournalDTO findByJournalDate(Date journalDate);

	@Query("{ 'userId' : ?0 , 'journalMonth' : ?1, 'journalYear': ?2 }")
    List<JournalDTO> findByMonthAndYear(String id, String month, String year);
}
