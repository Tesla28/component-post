package com.quot.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.DiaryDTO;

@Repository
public interface DiaryRepository extends MongoRepository<DiaryDTO, Integer>
{

	List<DiaryDTO> findByUserName(String username);

	DiaryDTO findById(String diaryId);

	@Query("{'_id' : ?0 , 'userName' : ?1 }")
	DiaryDTO findByIdAndUsername(String diaryId, String username);

}
