package com.quot.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.quot.api.dto.WriteUpDTO;

public interface WriteUpRespository extends MongoRepository<WriteUpDTO, Integer>
{

	WriteUpDTO findById(String id);

	void deleteById(String writeupId);

	@Query("{'username' : ?0}")
	List<WriteUpDTO> findAllByUsername(String username);

}
