package com.quot.api.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.PostDTO;

@Repository
public interface PostRepository extends MongoRepository<PostDTO, Integer>
{

	void deleteById(String postId);

	PostDTO findById(String postId);

	// List<PostDTO> findByUserId(String userId, Integer offset, Integer limit);

	Slice<PostDTO> findByUserId(String userId, Pageable pageable);

	@Query("{ 'userId' : ?0 , 'isDraft' : true }")
	List<PostDTO> findUserPostDrafts(String userId);
	
}
