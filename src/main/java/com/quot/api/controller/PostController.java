package com.quot.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.dto.AddPostRequestWrapper;
import com.quot.api.dto.CommentEditWrapper;
import com.quot.api.dto.CommentRequestWrapper;
import com.quot.api.dto.CreateJournalWrapper;
import com.quot.api.dto.EditWriteUpRequestDTO;
import com.quot.api.dto.JournalUpdateRequestDTO;
import com.quot.api.dto.PagedPostEditTextDTO;
import com.quot.api.dto.PostDTO;
import com.quot.api.dto.PostEditTextDTO;
import com.quot.api.dto.PostEditTitleDTO;
import com.quot.api.dto.PostsResponseWrapper;
import com.quot.api.dto.ReplyCommentRequestWrapper;
import com.quot.api.dto.WriteUpRequestDTO;
import com.quot.api.dto.WriteupResponseWrapper;
import com.quot.api.service.PostService;
import com.quot.api.service.TimelineService;

@RestController
@RequestMapping(path = "/quot/api/post")
public class PostController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PostController.class);

	@Autowired
	private AuthConsumer authConsumer;

	@Autowired
	private PostService postService;
	
	@Autowired 
	private TimelineService timelineService;

	/************** Post API Start **************/

	/**
	 * API to add a quoted post from a user
	 * 
	 * @author Akshay
	 * @param postRequest
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	// @CacheEvict(value = PostConstants.REDIS_USER_DASHBOARD_VALUE, allEntries = true)
	@RequestMapping(path = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity addPost(@RequestBody AddPostRequestWrapper addPostRequestWrapper, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in addPost controller method... addPostRequestWrapper : " + addPostRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.addPostService(addPostRequestWrapper, token);
		LOGGER.trace("Service response : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(412)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.PRECONDITION_FAILED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity(responseMap, HttpStatus.CREATED);
	}
	
	
	/**
	 * 
	 * API to delete a post by the user who quoted that post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(path = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deletePost(
			@RequestParam("postId") String postId, 
			@RequestHeader("Authorization") String token) throws Exception
	{
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.deletePostService(postId, token);
		if (responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(403))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if (responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Response from deletePost method : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/**
	 * 
	 * API to edit the non-paged post text by the user who quoted that post
	 * 
	 * @author Akshay
	 * @param postEditDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(path = "/text/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editPostText(@RequestBody PostEditTextDTO postEditTextDto,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in editPostText... postId : " + postEditTextDto.getPostId());
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.editPostTextService(postEditTextDto, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Response from editPostText : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to edit post text from a paged post on particular page number
	 * 
	 * @author Akshay
	 * @param postEditTextDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(path = "/paged/text/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editPagedPostText(@RequestBody PagedPostEditTextDTO pagedPostEditTextDto,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in editPagedPostText... postId : " + pagedPostEditTextDto.getPostId());
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.editPagedPostTextService(pagedPostEditTextDto, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Response from editPagedPostText : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to edit the post title by the user who quoted that post
	 * 
	 * @author Akshay
	 * @param postEditTitleDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/title/edit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editPostTitle(@RequestBody PostEditTitleDTO postEditTitleDto,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in editPostText... postId : " + postEditTitleDto.getPostId());
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.editPostTitleService(postEditTitleDto, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Response from editPostText : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/**
	 * 
	 * API to get the post information by the input postId param
	 * 
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getPostById(@RequestParam(name = "postId", required = true) String postId, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in getPostById controller");
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.getPostByIdService(postId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("response from getPostById : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/save/drafts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity saveDrafts(@RequestParam PostDTO post ,@RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in saveDrafts controller");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.saveDraftService(post, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.trace("response from saveDrafts : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to get user drafts
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/get/drafts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getDrafts(@RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in getDrafts controller");
		Map<String, Object> responseMap = new HashMap<>();
	
		responseMap = postService.getUerDraftsService(token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(412)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.PRECONDITION_FAILED);
		}
		
		LOGGER.trace("response from getDrafts : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get all posts of mentioned userId parameter
	 * 
	 * @author akshay
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/get/user/posts/{offset}/{limit}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUserPosts(@RequestParam(name = "userId", required = true) String userId, 
			@PathVariable(name = "offset", required = false) Integer offset,
			@PathVariable(name = "limit", required = false) Integer limit,
			@RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in getPostById controller");
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.getUserPostsService(userId, offset, limit, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.trace("response from getPostById : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/**
	 * API invoked on the like action by a user on a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param userName
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/upvote", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity upvotePost(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in upvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.upvotePostService(postId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from upvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to get all the users data who upvoted the post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/upvote/get/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUpvoteUsers(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in removeUpvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.getUpvoteUsersService(postId, token);
		System.out.println("responseMap : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from removeUpvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API invoked on the remove user upvote from a post
	 * 
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/upvote/remove", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity removeUpvotePost(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in removeUpvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.removeUpvotePostService(postId, token);
		System.out.println("responseMap : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from removeUpvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API invoked on the like action by a user on a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/appreciate", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity appreciatePost(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in upvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.appreciatePostService(postId, token);
		System.out.println("responseMap : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from upvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}	
	
	/**
	 * 
	 * API to get users data who appreciate the post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/appreciate/get/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAppreciateUsers(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in removeUpvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.getAppreciateUsersService(postId, token);
		System.out.println("responseMap : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from removeUpvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	
	/**
	 * 
	 * API invoked on the remove user appreciate from a post
	 * 
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/appreciate/remove", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity removeAppreciatePost(@RequestParam("postId") String postId, @RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in removeUpvotePost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.removeAppreciatePostService(postId, token);
		System.out.println("responseMap : " + responseMap);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		LOGGER.trace("Response from removeUpvotePost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	

	/**
	 * API invoked add comment on a post
	 * 
	 * @author Akshay
	 * @param commentRequestDTO
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity comment(@RequestBody CommentRequestWrapper commentRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in comment");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.commentService(commentRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Response from comment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API invoked to get all the comments on a post
	 * 
	 * 
	 * @author Akshay
	 * @param commentRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/get-all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAllCommentFromAPost(@RequestParam(name = "postId") String postId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getAllCommentFromAPost... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.getAllCommentFromAPostService(postId, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		
		LOGGER.trace("Response from getAllCommentFromAPost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get comment data from commentId
	 * 
	 * @author Akshay
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getCommentById(@RequestParam(name = "commentId") String commentId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getAllCommentFromAPost... commentId : " + commentId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.getCommentByIdService(commentId, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		
		LOGGER.trace("Response from getAllCommentFromAPost : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API invoked to edit specific comment from a post
	 * 
	 * @author Akshay
	 * @param editCommentDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editComment(@RequestBody CommentEditWrapper commentEditWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in editComment");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.editCommentService(commentEditWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Response from editComment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	
	/**
	 * 
	 * API invoked to delete specified comment on a post
	 * 
	 * @author Akshay
	 * @param postId
	 * @param commentId
	 * @param userName
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	@RequestMapping(path = "/comment/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteComment(@RequestParam(value = "postId", required = true) String postId,
			@RequestParam(value = "commentId", required = true) String commentId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in deleteComment... commentId : " + commentId + " ,postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.deleteCommentService(postId, commentId, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to add reply to a comment
	 * 
	 * @author Akshay
	 * @param commentRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/reply", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity replyOnComment(@RequestBody ReplyCommentRequestWrapper replyCommentRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in replyOnComment");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.replyCommentService(replyCommentRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Response from replyOnComment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to edit reply on a comment
	 * 
	 * @author Akshay
	 * @param replyCommentRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/reply/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity EditReplyOnComment(@RequestBody ReplyCommentRequestWrapper replyCommentRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in EditReplyOnComment");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.editReplyCommentService(replyCommentRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Response from EditReplyOnComment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to delete a reply on the comment
	 * 
	 * @author Akshay
	 * @param commentId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/comment/reply/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteReplyOnComment(@RequestParam(name = "commentId") String commentId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in deleteReplyOnComment... commentId : " + commentId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.deleteReplyCommentService(commentId, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Response from deleteReplyOnComment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/get/likes-and-comments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getLikesAndCommentsCount(@RequestParam(name = "postId") String postId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in deleteReplyOnComment... postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.getLikesAndCommentsCountService(postId, token);
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		
		LOGGER.trace("Response from deleteReplyOnComment : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	
	/**
	 * 
	 * API invoked to get all the quote posts present in database
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(path = "/posts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public PostsResponseWrapper getAllPosts(@RequestParam(name = "username", required = true) String username,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getAllPosts controller method");
		PostsResponseWrapper responseWrapper = new PostsResponseWrapper();
		
		// responseWrapper = postService.getAllPostServices(username,token);
		if(responseWrapper.getStatusCode() == 500)
		{
			LOGGER.trace("Server error");
			// return new ResponseEntity<PostsResponseWrapper>(responseWrapper, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseWrapper.getStatusCode() == 204)
		{
			LOGGER.trace("No Content");
			// return new ResponseEntity<PostsResponseWrapper>(responseWrapper, HttpStatus.NO_CONTENT);
		}
		else if(responseWrapper.getStatusCode() == 403)
		{
			// return new ResponseEntity<PostsResponseWrapper>(responseWrapper, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("response from getAllPosts : " + responseWrapper);
		//return new ResponseEntity<PostsResponseWrapper>(responseWrapper, HttpStatus.OK);
		return responseWrapper;
	}
	

	/**
	 * 
	 * API invoked to get all the posts of currently loggedin user
	 * 
	 * @author Akshay
	 * @param userName
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(path = "/myPosts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getMyPosts(@RequestParam String userName, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("in getMyPosts controller method... userName : " + userName);
		Map<String, Object> responseMap = new HashMap<>();

		boolean isTokenValid = authConsumer.isTokenValid(token.substring(7));
		if(!isTokenValid)
		{
			LOGGER.trace("Unauthorized");
			responseMap.put("message", "Unauthorized");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		responseMap = postService.getCurrentUserPosts(userName, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(409))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from getMyPosts controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/************** Post API End **************/

	/************** User timeline API Start **************/
	
	
	/**
	 * 
	 * API to get user's home timeline
	 * 
	 * @author Akshay
	 * @param userName
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	@RequestMapping(path = "/home-timeline", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity homeTimeline(
			@RequestParam(name = "offset", required = true) int offset, 
			@RequestParam(name = "limit", required = true) int limit, 
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in homeTimeline controller method");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = timelineService.getHomeTimelineService(token, offset, limit);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(406)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_ACCEPTABLE);
		}
		else if (responseMap.get("statusCode").equals(new Integer(409)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Respone from homeTimeline controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get user's user timeline
	 * 
	 * @author Akshay
	 * @param offset
	 * @param limit
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	@RequestMapping(path = "/user-timeline", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity userTimeline(
			@RequestParam(name = "offset", required = true) int offset, 
			@RequestParam(name = "limit", required = true) int limit, 
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in userTimeline controller method");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = timelineService.getUserTimelineService(token, offset, limit);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(406)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_ACCEPTABLE);
		}
		else if (responseMap.get("statusCode").equals(new Integer(409)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Respone from userTimeline controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/************** User Home timeline API End **************/
	

	/************** Journal API Start **************/

	/**
	 * 
	 * API to add a journal
	 * 
	 * @author Akshay
	 * @param journalRequestDTOString
	 * @param imageFiles
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(path = "/journal/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> createJournal(@RequestBody CreateJournalWrapper createJournalWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("In Controller method of create journal... journalRequestDto : " + createJournalWrapper.toString());
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.createJournalService(createJournalWrapper, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(406))
        {
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_ACCEPTABLE);
        }
		else if(responseMap.get("statusCode").equals(403))
        {
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
        }

		LOGGER.trace("Respone from createJournal controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.CREATED);
	}

	
	/**
	 * 
	 * API to delete a journal
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(path = "/journal/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> deleteJournal(
			@RequestParam(required = true, value = "journalId") String journalId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("In Controller method of delete journal... journalId : " + journalId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.deleteJournalService(journalId, token);
		if(responseMap.get("statusCode").equals(500))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from deleteJournal controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(path = "/journal/get-by-month-and-year", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getMonthJournals(
            @RequestParam(required = true, value = "month") String month,
            @RequestParam(required = true, value = "year") String year,
            @RequestHeader("Authorization") String token) throws Exception
    {
        LOGGER.trace("In Controller method of delete journal... month : " + month + ", year : " + year);
        Map<String, Object> responseMap = new HashMap<>();
        
        responseMap = postService.getMonthJournalsService(month, year, token);
        if(responseMap.get("statusCode").equals(500))
        {
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else if(responseMap.get("statusCode").equals(401))
        {
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
        }
        else if(responseMap.get("statusCode").equals(204))
        {
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
        }
        
        LOGGER.trace("Respone from deleteJournal controller : " + responseMap);
        return new ResponseEntity(responseMap, HttpStatus.OK); 
    }
	

	/**
	 * 
	 * Controller method to edit a user journal
	 * 
	 * @author Akshay
	 * @param diaryId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(path = "/journal/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> updateJournal(
			@RequestBody JournalUpdateRequestDTO journalUpdateRequestDto, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of delete journal... journalUpdateRequestDto : " + journalUpdateRequestDto);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.updateJournalService(journalUpdateRequestDto, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
        {
            LOGGER.trace("User is forbidden to use the service");
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
        }
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from updateJournal controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	
	/**
	 * 
	 * Controller method to get journal information
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	@RequestMapping(path = "/journal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> getUserJournal(@RequestParam(name = "journalId") String journalId, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method getUserJournal... journalId : " + journalId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.getUserJournalService(journalId, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(404))
        {
            LOGGER.trace("Journal data not found");
            return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
        }
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from getJournal controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * Controller method to make a user journal public
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	@RequestMapping(path = "/journal/share", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> makeJournalPublic(@RequestParam(name = "journalId") String journalId,
			@RequestParam(name = "username") String username, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of make journal... journalId : " + journalId);
		Map<String, Object> responseMap = new HashMap<>();
		
		// responseMap = postService.makeJournalPublic(journalId, username, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Respone from makeJournalPublic controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/**
	 * 
	 * Controller method to like a shared journal
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	@RequestMapping(path = "/journal/like", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> likeJournal(@RequestParam(name = "journalId") String journalId,
			@RequestParam(name = "username") String username, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of likeJournal... journalId : " + journalId);
		Map<String, Object> responseMap = new HashMap<>();
		
		// responseMap = postService.likeJournalService(journalId, username, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Respone from likeJournal controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}

	/************** Journal API End **************/

	/************** WriteUp API Start **************/

	/**
	 * 
	 * Controller method to create user writeup
	 * 
	 * @author Akshay
	 * @param writeUpRequest
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	// @CachePut(value = PostConstants.CACHE_WRITEUP)
	@RequestMapping(path = "/writeup/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> createUserWriteup(@RequestBody WriteUpRequestDTO writeUpRequest,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("In Controller method of createUserWriteup... writeUpRequest : " + writeUpRequest);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.createUserWriteupService(writeUpRequest, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from createUserWriteup controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.CREATED);
	}
	
	/**
	 * 
	 * Controller method to get a user writeup or writeup list
	 * 
	 * @author Akshay
	 * @param writeUpRequest
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	@RequestMapping(path = "/writeup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> getUserWriteup(
			@RequestParam(name = "writeupId", required = true) String writeupId,
			@RequestParam(name = "username", required = true) String username,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("In Controller method of getUserWriteup... writeupId : " + writeupId);
		Map<String, Object> responseMap = new HashMap<>();
		
		if(writeupId != null && writeupId!="")
		{
			LOGGER.trace("writeupId is present in the request : " + writeupId);
			responseMap = postService.getUserWriteupByIdService(writeupId, token);
		}
		else
		{
			LOGGER.trace("writeupId is not present in the request, hence getting all the writeups for username : " + username);
			responseMap = postService.getUserWriteupByUsernameService(username, token);
		}
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content found for the writeup");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from getUserWriteup controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);

	}
	
	/**
	 * 
	 * Controller to edit a user writeup
	 * 
	 * @author Akshay
	 * @param writeupId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	// @CachePut(value = PostConstants.CACHE_QUOTE, key = "#editWriteUpRequestDto.writeupId")
	@RequestMapping(path = "/writeup/edit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> editUserWriteup(@RequestBody EditWriteUpRequestDTO editWriteUpRequestDto,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("In Controller method of editUserWriteup... editWriteUpRequestDto : " + editWriteUpRequestDto);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.editUserWriteupService(editWriteUpRequestDto, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content found for the writeup");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Respone from editUserWriteup controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * Controller method to delete a user writeup
	 * 
	 * @author Akshay
	 * @param editWriteUpRequestDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	// @CacheEvict(value = PostConstants.CACHE_WRITEUP)
	@RequestMapping(path = "/writeup/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> deleteUserWriteup(@RequestParam(name = "writeupId") String writeupId,
			@RequestParam(name = "username") String username, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of deleteUserWriteup... writeupId : " + writeupId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = postService.deleteUserWriteupService(writeupId, username, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content found for the writeup");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Respone from deleteUserWriteup controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Controller method to like a user writeup
	 * 
	 * @author Akshay
	 * @param writeupId
	 * @param username
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	// @CachePut(value = PostConstants.CACHE_QUOTE, key = "#writeupId")
	@RequestMapping(path = "/writeup/like", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> likeUserWriteup(@RequestParam(name = "writeupId") String writeupId,
			@RequestParam(name = "username", required = false) String username, @RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of likeUserWriteup... writeupId : " + writeupId + " , username : " + username);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = postService.likeUserWriteupService(writeupId, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.error("Server errror");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(400))
		{
			LOGGER.trace("Bad request");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content found for the writeup");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("User is forbidden to use the service");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Respone from likeUserWriteup controller : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Controller method to get user writeups
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(path = "/writeups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WriteupResponseWrapper getAllWriteups(@RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In Controller method of getAllWriteups");
		WriteupResponseWrapper responseWrapper = new WriteupResponseWrapper();
		
		responseWrapper = postService.getAllWriteupsService(token);
		if(responseWrapper.getStatusCode() == 500)
		{
			LOGGER.error("Server errror");
		}
		else if(responseWrapper.getStatusCode() == 204)
		{
			LOGGER.trace("No Content");
		}
		else if(responseWrapper.getStatusCode() == 403)
		{
			LOGGER.trace("Forbidden");
		}
		
		LOGGER.trace("Respone from getAllWriteups controller : " + responseWrapper);
		return responseWrapper;
	}
	
	
	/************** WriteUp API End **************/
	
	
	/************** Cache API End **************/
	
	
	/**
	 * 
	 * API to clear Redis server saved cache
	 * 
	 * 
	 * TO-DO
	 * @author Akshay
	 * @param token
	 * @throws Exception
	 */
	@Deprecated
	// @CacheEvict(value = PostConstants.REDIS_USER_DASHBOARD_VALUE, allEntries = true)
	@RequestMapping(path = "/cache/clear", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void cacheClear(@RequestHeader("Authorization") String token)
			throws Exception
	{
		LOGGER.trace("In cacheClear controller method");
	}
	
	/************** Cache API End **************/
}
