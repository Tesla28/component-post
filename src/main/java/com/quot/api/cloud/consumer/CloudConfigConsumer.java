package com.quot.api.cloud.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.quot.api.dto.DeleteImagesRequestDTO;

@Service
public class CloudConfigConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudConfigConsumer.class);

	@Autowired
	private CloudConfigClient cloudConfigClient;

	/**
	 * 
	 * Consumer method of component cloud's bulkUploadImages method
	 * 
	 * @author Akshay
	 * @param username
	 * @param journalId
	 * @param imageFiles
	 * @param token
	 */
	@SuppressWarnings("rawtypes")
	public void bulkUploadJournalImages(String username, String journalId, MultipartFile[] imageFiles, String token)
	{
		LOGGER.trace("in CloudConfigConsumer bulkUploadJournalImages method");
		ResponseEntity response = null;

		try
		{
			response = cloudConfigClient.bulkUploadJournalImages(imageFiles, username, journalId, token);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Images uploaded to user journal");
			}
		}
		catch (Exception err)
		{
			response = null;
			LOGGER.error("Error in CloudConfigClient : " + err.getMessage());
		}
	}
	
	/**
	 * 
	 * Consumer method of component cloud's deleteJournalFolder method
	 * 
	 * @author Akshay
	 * @param username
	 * @param journalId
	 * @param token
	 */
	@SuppressWarnings("rawtypes")
	public void deleteJournalFolder(String username, String journalId, String token)
	{
		LOGGER.trace("in CloudConfigConsumer deleteJournalFolder method");
		ResponseEntity response = null;
		try
		{
			response = cloudConfigClient.deleteJournalFolder(username, journalId, token);
			LOGGER.trace("Response from cloudConfig component : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Images delete from user journal");
			}
		}
		catch (Exception err)
		{
			response = null;
			LOGGER.error("Error in CloudConfigClient : " + err.getMessage());
		}
	}

	/**
	 * 
	 * Consumer method of component cloud's deleteJournalImages method
	 * 
	 * @author Akshay
	 * @param username
	 * @param journalId
	 * @param imagesList
	 * @param token
	 */
	@SuppressWarnings("rawtypes")
	public void deleteJournalFolderImages(DeleteImagesRequestDTO deleteImagesRequestDtos, String token)
	{
		LOGGER.trace("in CloudConfigConsumer deleteJournalFolderImages method");
		ResponseEntity response = null;
		try
		{
			response = cloudConfigClient.deleteJournalFolderImages(deleteImagesRequestDtos, token);
			LOGGER.trace("Response from cloudConfig component : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Images delete from user journal");
			}
		}
		catch (Exception err)
		{
			response = null;
			LOGGER.error("Error in CloudConfigClient : " + err.getMessage());
		}
	}
}
