package com.quot.api.cloud.consumer;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.quot.api.dto.DeleteImagesRequestDTO;

import feign.Headers;

@FeignClient(name = "COMPONENT-CLOUD", url = "localhost:7041")
public interface CloudConfigClient
{
	@Headers("Content-Type: multipart/form-data")
	@RequestMapping(path = "/quot/api/cloud/journal/image/bulkUpload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Map<String, Object>> bulkUploadJournalImages(@RequestPart(value = "images") MultipartFile imageFiles[],
			@RequestPart("username") String username, @RequestPart("journalId") String journalId,
			@RequestHeader("Authorization") String token);
	
	@RequestMapping(path = "/quot/api/cloud/journal/delete/folder/allImages", method = RequestMethod.DELETE)
	public ResponseEntity<Map<String, Object>> deleteJournalFolder(@RequestParam("username") String username,
			@RequestParam("journalId") String journalId, @RequestHeader("Authorization") String token);

	@SuppressWarnings("rawtypes")
	@RequestMapping(path = "/quot/api/cloud/journal/delete/folder/images", method = RequestMethod.POST)
	public ResponseEntity deleteJournalFolderImages(@RequestBody DeleteImagesRequestDTO deleteImagesRequestDto, @RequestHeader("Authorization") String token);
	
}
